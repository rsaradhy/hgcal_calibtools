import uproot as up
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats as st
import pandas as pd
from matplotlib.colors import LogNorm
from matplotlib.gridspec import GridSpec
import scipy.optimize as opt


import matplotlib as mpl
# import pylandau as landau
mpl.rcParams['image.cmap'] = 'inferno'
from scipy.optimize import curve_fit
from scipy.stats import iqr, norm, binned_statistic, binned_statistic_2d
from scipy.special import erf
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)




def drawCombinedTOF(eng_TOF_F,eng_TOF_R):
    TOF1=np.array(eng_TOF_F)
    TOF2 = np.array(eng_TOF_R)
    f, axs = plt.subplots(1,2, figsize = (16,6))


    #For Fall


    bins=np.arange(-2,2,0.01)
    y,bins = np.histogram(TOF1,bins=bins)
    x = bins[:-1]+(bins[1]-bins[0])/2
    mean = sum(x * y) / sum(y)
    sigma = np.sqrt(sum(y * (x - mean)**2) / sum(y))
    #mean = np.mean(TOF1)
    #sigma = np.std(TOF1)
    

    x = bins[:-1]+(bins[1]-bins[0])/2
    _ = axs[0].hist(TOF1,bins=bins)
    # _ = axs.scatter(x,y,color="orange")
    popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,0.1])
    fall_par = popt
    fall_err = np.sqrt(np.diag(pcov))

    _=axs[0].plot(x,gaus(x,*popt),'r--',label='fit')
    #axs.set_xlim(8,14)
    axs[0].set_xlabel("Corrected TOF Fall (ns)",fontsize=15)

    textstr = f"Mean: {round(popt[1],3)} +/- {round(fall_err[1],3)} ns\n"
    textstr += f"Sig: {np.abs(round(popt[2],3))} +/- {round(fall_err[2],3)} ns\n"
    textstr += f"Events: {TOF1.size} \n"



    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    _ =axs[0].text(0.6, 0.99,textstr,
             transform=axs[0].transAxes,
             fontsize=15,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")



    #For Rise

    y,bins = np.histogram(TOF2,bins=bins)
    x = bins[:-1]+(bins[1]-bins[0])/2
    mean = sum(x * y) / sum(y)
    sigma = np.sqrt(sum(y * (x - mean)**2) / sum(y))
    #     mean = np.mean(TOF2)
    #     sigma = np.std(TOF2)

    x = bins[:-1]+(bins[1]-bins[0])/2
    _ = axs[1].hist(TOF2,bins=bins)
    # _ = axs.scatter(x,y,color="orange")
    popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])

    rise_par = popt
    rise_err = np.sqrt(np.diag(pcov))


    _=axs[1].plot(x,gaus(x,*popt),'r--',label='fit')
    #     axs.set_xlim(8,14)
    axs[1].set_xlabel("Corrected TOF Rise (ns)",fontsize=15)


    textstr = f"Mean: {round(popt[1],3)}  +/- {round(rise_err[1],3)}ns\n"
    textstr += f"Sig: {np.abs(round(popt[2],3))}  +/- {round(rise_err[2],3)}ns\n"
    textstr += f"Events: {TOF2.size} \n"

    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    _ =axs[1].text(0.6, 0.99,textstr,
             transform=axs[1].transAxes,
             fontsize=15,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")
    return ((fall_par,fall_err),(rise_par,rise_err))


# def fit_function(x,a,b,c):
    # return np.sqrt((a/np.sqrt(x-b))**2+c**2)
# def fit_function(x,a,b,c,d):
#     return np.sqrt((a/np.sqrt(x-b))**2+(d/np.sqrt(x-b))**2+c**2)

def drawSigma(p_all):
    def fit_function(x,a,b,c):
        return np.sqrt((a/np.sqrt(x-b))**2+c**2)

    x1,y1,x2,y2 = [],[],[],[]
    x1_err,y1_err,x2_err,y2_err = [],[],[],[]

    for be,(p_fall,p_rise) in p_all:
        x1.append(be)
        x2.append(be)    
        y1.append(np.abs(p_fall[0][2]))
        y1_err.append(p_fall[1][2])
        y2.append(np.abs(p_rise[0][2]))
        y2_err.append(p_rise[1][2])


    x1 = np.asarray(x1)
    y1 = np.asarray(y1)
    x2 = np.asarray(x2)
    x11 = x1[0:]
    y11 = y1[0:]
    y11_err = y1_err[0:]
    x22 = x2[0:]
    y22 = y2[0:]
    y22_err = y2_err[0:]


    xpos,ypos=1.05,.5 # controls the position of the text boxes

    xx=np.linspace(80,300,100)


    # f, axs = plt.subplots(1,2, figsize = (16,6))
    f, axs = plt.subplots( figsize = (8,6))


    # axs.set_title("TOA Fall",fontsize=15)
    axs.set_xlabel("Beam Energy [GeV]",fontsize=15)
    axs.set_ylabel(r"$\sigma$ [ns]",fontsize=15)

    # axs[1].set_title("TOA Rise",fontsize=15)
    # axs[1].set_xlabel("E [GeV]",fontsize=15)
    # axs[1].set_ylabel(r"$\sigma$ [ns]",fontsize=15)

    par,par_err = curve_fit(fit_function,x11,y11,p0=[3,57,0.121],sigma=y11_err)
    par_err = np.sqrt(np.diag(par_err))

    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)

    parameter_name = ['A','B','C','d']
    textstr = r"$Fall =\sqrt{(\frac{A}{\sqrt{E-B}})^{2}+ C^{2} }$"+"\n"
    for i,j,k in zip(par,parameter_name,par_err):
        textstr+=f" {j} = {round(i,3)}"+r"$\pm$"+f"{round(k,3)}\n"
    axs.text(xpos, ypos,textstr,
         transform=axs.transAxes,
         fontsize=12,
         verticalalignment='top',
         multialignment='center',
         bbox=props,
         label="Cuts")

    fit_function(xx,*par)
    _=axs.plot(xx,fit_function(xx,*par),linewidth=3,linestyle=":",label="Fit Falling Edge",zorder=20)
    color = _[0].get_color()

    # axs.fill_between(xx,fit_function(xx,*(par+par_err)),fit_function(xx,*(par-par_err)),alpha=.5,label="Error in Fit Fall",color=color,zorder=18)
    axs.errorbar(x1,y1,yerr=y1_err,fmt='o',color='black',
                        capsize=5,
                        ecolor='red'
                        ,label='Falling Edge'
                        ,zorder=21
                       )

    par,par_err = curve_fit(fit_function,x22,y22,p0=[3,62,0.121],sigma=y22_err)
    par_err = np.sqrt(np.diag(par_err))

    # textstr = r"$Rise =\sqrt{(\frac{a}{\sqrt{x-b}})^{2}+ c^{2} }$"+"\n"
    textstr = r"$Rise =\sqrt{(\frac{A}{\sqrt{E-B}})^{2}+ C^{2} }$"+"\n"

    for i,j,k in zip(par,parameter_name,par_err):
        textstr+=f" {j} = {round(i,3)}"+r"$\pm$"+f"{round(k,3)}\n"

    axs.text(xpos, ypos+.4,textstr,
         transform=axs.transAxes,
         fontsize=12,
         verticalalignment='top',
         multialignment='center',
         bbox=props,
         label="Cuts")


    _ = axs.plot(xx,fit_function(xx,*par),linewidth=3,linestyle=":",label="Fit Rising Edge",zorder=20)
    color = _[0].get_color()

    # axs.fill_between(xx,fit_function(xx,*(par+par_err)),fit_function(xx,*(par-par_err)),alpha=.5,label="Error in Fit Rise",color=color,zorder=19)

    axs.errorbar(x2,y2,yerr=y2_err,fmt='o',color='green',
                        capsize=5,
                        ecolor='red'
                        ,label='Rising Edge'
                        ,zorder=21
                       )

    axs.xaxis.set_major_locator(MultipleLocator(50))
    axs.xaxis.set_minor_locator(MultipleLocator(10))
    axs.grid(which='major')
    axs.minorticks_on()

    axs.legend(loc="upper right")
        #Formatting
    axs.text(0,1.08 , r"$\bf{CMS}$ Preliminary",
         transform=axs.transAxes,
         fontsize=22,
         verticalalignment='top',
         multialignment='center',
         # bbox=props,
         # zorder=15,
         label="cms")

    
    textDisp = r""
    
    _ = axs.text(0.6, 1.08,textDisp,
      transform=axs.transAxes,
      fontsize=18,
      verticalalignment='top',
      multialignment='center',
      # bbox=props,
      # zorder=15,
      label="lumi")




def gaus(x,a,x0,sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

def chi2(fit):
    return np.sum(fit.fun**2)/(fit.fun.size -fit.x.size )

def plotTimeResvsEdep(x1,y1,y1_err,axs,label="Test",offset=0):
    
    xpos,ypos=1.05,.5 # controls the position of the text boxes


    xx=np.linspace(x1[0],x1[-1],100)

    # axs.set_xlabel("Energy Deposited [MIP]",fontsize=15)
    axs.set_ylabel(r"$\sigma$ [ns]",fontsize=15)

    parameter_name = ['A','B','C','d']
    textstr = f"{label}"+r"$=\sqrt{(\frac{A}{E-B})^{2}+ C^{2} }$"+"\n"
    def fit_func(x,a,b,c):
         return np.sqrt(((a/(x-b))**2+c**2))
        #  return np.sqrt((a/np.sqrt(x-b))**2+c**2)
    
    


    residual=lambda par,x,y,yErr: (fit_func(x,*par)-y)/(yErr)
    initPar = [5,5,0.100]
    fit_obj = opt.least_squares(residual,initPar,args=(x1,y1,y1_err))


    par = fit_obj.x
    J=fit_obj.jac
    pcov = np.linalg.inv(J.T.dot(J))
    par_err = np.sqrt(np.diag(pcov))

    # par,par_err = curve_fit(fit_function,x11,y11,p0=[5,10,0.200],sigma = (y1_err))
    # par_err = np.sqrt(np.diag(par_err))

    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)

    parameter_name = ['A','B','C','d']
    textstr = f"{label}"+r"$=\sqrt{(\frac{A}{E-B})^{2}+ C^{2} }$"+"\n"
    for i,j,k in zip(par,parameter_name,par_err):
        textstr+=f" {j} = {round(i,3)}"+r"$\pm$"+f"{round(k,3)}\n"
    
    textstr+=r"$\chi^{2}_{\nu}$"+f" = {round(chi2(fit_obj),3)}"
    
    axs.text(xpos, ypos+offset,textstr,
         transform=axs.transAxes,
         fontsize=12,
         verticalalignment='top',
         multialignment='center',
         bbox=props,
         label="Cuts")

    _=axs.plot(xx,fit_func(xx,*par),linewidth=3,linestyle=":",label=f"Fit {label}",zorder=20)
    color = _[0].get_color()

    # axs.fill_between(xx,fit_function(xx,*(par+par_err)),fit_function(xx,*(par-par_err)),alpha=.5,label="Error in Fit Fall",color=color,zorder=18)
    _ = axs.errorbar(x1,y1,yerr=y1_err,fmt='o',
                        color=color,
                        capsize=5
                       ,ecolor=color
                        ,label=f'{label}'
                        ,zorder=21
                       )

    # color = _[0].get_color()

#     axs.scatter(x1,y1,marker='o',color='black'
#                         ,label='Fall Edge'
#                         ,zorder=21
#                        )



    axs.xaxis.set_major_locator(MultipleLocator(100))
    axs.xaxis.set_minor_locator(MultipleLocator(50))
    # axs.minorticks_on()

    axs.legend(loc="upper right")
        #Formatting
    # axs.text(0,1.08 , r"$\bf{CMS}$ Preliminary",
    #      transform=axs.transAxes,
    #      fontsize=22,
    #      verticalalignment='top',
    #      multialignment='center',
    #      # bbox=props,
    #      # zorder=15,
    #      label="cms")


#     textDisp = r"Fall Merged_MCP > 12.5" if phaseSelect else  r"Fall Merged_MCP < 12.5"
    # textDisp = r"Fall MCP > 12.5 Rise MCP > 12.5 ns"

#     _ = axs.text(0.6, 1.08,textDisp,
#       transform=axs.transAxes,
#       fontsize=18,
#       verticalalignment='top',
#       multialignment='center',
#       # bbox=props,
#       # zorder=15,
#       label="lumi")
    return color,fit_obj