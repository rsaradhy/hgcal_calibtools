import uproot as up
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats as st
from matplotlib.colors import LogNorm
import matplotlib as mpl
from scipy.optimize import curve_fit
from scipy.stats import iqr, norm, binned_statistic, binned_statistic_2d
from scipy.special import erf
from Calibration.calib_tools.ct_pandas import *
# from calib_tools.calib_tools import *



def time_res_version():
    test()
    version=1.2
    print(f"Time Res Version {version}")

def CalculateTOF(X1,Y1,X2,Y2,Layer,par,correctMean=1):#X is energy, Y is TOA-MCP, p is fit parameters
    p= par[0][0]
    TOF1 = Y1 - p[2]/(X1-p[3]) - p[0]*X1  -p[1]
    p= par[1][0]
    TOF2 = Y2 - p[2]/(X2-p[3]) - p[0]*X2 -p[1]
        
    
    f, axs = plt.subplots(1,2, figsize = (16,6))

    
    #For Fall
    
  
    bins=250
    y,bins = np.histogram(TOF1,bins=bins,weights=X1)

    mean = np.mean(TOF1)
    sigma = np.std(TOF1)

    x = bins[:-1]+(bins[1]-bins[0])/2
    _ = axs[0].hist(TOF1,bins=bins,weights=(X1))
    # _ = axs.scatter(x,y,color="orange")
    popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
    fall_par = popt
    fall_err = np.sqrt(np.diag(pcov))
    
    _=axs[0].plot(x,gaus(x,*popt),'r--',label='fit')
    #axs.set_xlim(8,14)
    axs[0].set_xlabel("Corrected TOF Fall (ns)",fontsize=15)

    textstr = f"Mean: {round(popt[1],3)} +/- {round(fall_err[1],3)} ns\n"
    textstr += f"Sig: {round(popt[2],3)} +/- {round(fall_err[2],3)} ns\n"
    textstr += f"Events: {TOF1.size} \n"
    

    
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    _ =axs[0].text(0.3, 0.95,textstr,
             transform=axs[0].transAxes,
             fontsize=15,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")
    
    
    
    #For Rise
    
    y,bins = np.histogram(TOF2,bins=bins,weights=(X2))

    mean = np.mean(TOF2)
    sigma = np.std(TOF2)

    x = bins[:-1]+(bins[1]-bins[0])/2
    _ = axs[1].hist(TOF2,bins=bins,weights=(X2))
    # _ = axs.scatter(x,y,color="orange")
    popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
    
    rise_par = popt
    rise_err = np.sqrt(np.diag(pcov))
    
    
    _=axs[1].plot(x,gaus(x,*popt),'r--',label='fit')
    #     axs.set_xlim(8,14)
    axs[1].set_xlabel("Corrected TOF Rise (ns)",fontsize=15)

    
    textstr = f"Mean: {round(popt[1],3)}  +/- {round(rise_err[1],3)}ns\n"
    textstr += f"Sig: {round(popt[2],3)}  +/- {round(rise_err[2],3)}ns\n"
    textstr += f"Events: {TOF2.size} \n"
    
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    _ =axs[1].text(0.3, 0.95,textstr,
             transform=axs[1].transAxes,
             fontsize=15,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")
    
    if correctMean:
        TOF1-= fall_par[1]
        TOF2-= rise_par[1]
        
        
    
    
    
    #       TOF , mean  ,sigma
    return (TOF1,fall_par,fall_err),(TOF2,rise_par,rise_err)
    
def plot_TOFALL(TOF_Fall,Fall_Eng,TOF_Rise,Rise_Eng):


    X1 = np.concatenate(Fall_Eng)
    TOF1 = np.concatenate(TOF_Fall)
    weight1 = np.ones(X1.size)  #   X1/np.sum(X1)

    # X1 = np.concatenate(Fall_Eng)
    # TOF1 = np.concatenate(TOF_Fall)*X1/np.sum(X1)
    # weight1 = np.ones(X1.size)#X1/np.sum(X1)
    f, axs = plt.subplots(1,2, figsize = (16,6))
    bins=500
    y,bins = np.histogram(TOF1,bins=bins,weights=weight1)
    mean = np.mean(TOF1)
    sigma = np.std(TOF1)

    x = bins[:-1]+(bins[1]-bins[0])/2
    _ = axs[0].hist(TOF1,bins=bins,weights=weight1)
    #_ = axs.scatter(x,y,color="orange")
    popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
    fall_par = popt
    fall_err = np.sqrt(np.diag(pcov))
    _=axs[0].plot(x,gaus(x,*popt),'r--',label='fit')
    #axs.set_xlim(8,14)
    axs[0].set_xlabel("Corrected TOF Fall (ns)",fontsize=15)

    textstr = f"Mean: {round(popt[1],3)} +/- {round(fall_err[1],3)} ns\n"
    textstr += f"Sig: {round(popt[2],3)} +/- {round(fall_err[2],3)}ns\n"
    textstr += f"Evnts: {X1.size}\n"


    
    
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    _ =axs[0].text(0.1, 0.95,textstr,
             transform=axs[0].transAxes,
             fontsize=15,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")
     #For Rise
    TOF2 = np.concatenate(TOF_Rise)
    X2 = np.concatenate(Rise_Eng)
    weight2 = np.ones(X2.size)  #   X1/np.sum(X1)

    # weight2 = X2/np.sum(X2)

    
    y,bins = np.histogram(TOF2,bins=bins,weights=weight2)

    mean = np.mean(TOF2)
    sigma = np.std(TOF2)

    x = bins[:-1]+(bins[1]-bins[0])/2
    _ = axs[1].hist(TOF2,bins=bins,weights=weight2)
    # _ = axs.scatter(x,y,color="orange")
    popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
    
    rise_par = popt
    rise_err = np.sqrt(np.diag(pcov))
    
    
    _=axs[1].plot(x,gaus(x,*popt),'r--',label='fit')
    axs[1].set_xlabel("Corrected TOF Rise (ns)",fontsize=15)

    textstr = f"Mean: {round(popt[1],3)} +/- {round(rise_err[1],3)} ns\n"
    textstr += f"Sig: {round(popt[2],3)} +/- {round(rise_err[2],3)}ns\n"
    textstr += f"Evnts: {X2.size}\n"
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    _ =axs[1].text(0.1, 0.95,textstr,
             transform=axs[1].transAxes,
             fontsize=15,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")

    
    
    axs[1].set_xlim(-5,5)
    axs[0].set_xlim(-5,5)
    
    f.suptitle("Time Resolution Across All Layers")
    return (fall_par,fall_err),(rise_par,rise_err)
 

def plotSigmaLayer(p2,parameter=2):#Plotting Sigma vs Layers
    x,y1,y1_err,y2,y2_err = [],[],[],[],[]
    for layer,p1,p2 in p2:
        # if layer == 16:
        #     continue
        x.append(layer)
        y1.append(np.abs(p1[0][parameter]))
        y1_err.append(p1[1][parameter])
        y2.append(np.abs(p2[0][parameter]))
        y2_err.append(p2[1][parameter])
        
        
    f, axs = plt.subplots(1,2, figsize = (16,6))
    axs[0].errorbar(x,y1,yerr=y1_err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[0].set_xlabel("Layer",fontsize=15)
    axs[0].set_ylabel("Sigma of Corrected TOF",fontsize=15)
    axs[1].set_xlabel("Layer",fontsize=15)
    axs[1].set_ylabel("Sigma ",fontsize=15)
    
    axs[0]
    
    axs[1].errorbar(x,y2,yerr=y2_err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[1].set_ylim(0.1,.25)
    axs[0].set_ylim(.1,.25)
    f.suptitle("Energy Weighted TOF",fontsize=15)
    return

def plotPol1_AfterSlopeParameter(p): #Plotting Parameters after Slope Fix
    parameter = 0 # Plotting Slope
    x,y1,y1err,y2,y2err=[],[],[],[],[]
    for layer,_,_,_,rise,fall in p:
        if layer < 19:
            x.append(layer)
            y1.append(fall[0][parameter])
            y1err.append(fall[1][parameter])
            y2.append(rise[0][parameter])
            y2err.append(rise[1][parameter])
            
    
    f, axs = plt.subplots(1,2, figsize = (16,6))
    axs[0].errorbar(x,y1,yerr=y1err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[1].errorbar(x,y2,yerr=y2err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[0].set_xlabel("Layer",size=12)
    axs[0].set_ylabel("Slope",size=12)
    axs[0].set_title("TOA Fall",size=14)
    axs[1].set_xlabel("Layer",size=12)
    axs[1].set_ylabel("Slope",size=12)
    axs[1].set_title("TOA Rise",size=14)
    
    
    
    parameter = 1 # Plotting Intercept
    x,y1,y1err,y2,y2err=[],[],[],[],[]
    for layer,_,_,_,rise,fall in p:
        if layer != 7 and layer != 10 and layer < 19:
            x.append(layer)
            y1.append(fall[0][parameter])
            y1err.append(fall[1][parameter])
            y2.append(rise[0][parameter])
            y2err.append(rise[1][parameter])
            
    x= np.array(x)
    f, axs = plt.subplots(1,2, figsize = (16,6))
    par1 =  np.polyfit(x,y1,1)
    axs[0].plot(x,pol(x,par1),color="red",linestyle="--",  label=f'pol1 a={round(par1[0],3)}, b={round(par1[1],3)}')
    axs[0].errorbar(x,y1,yerr=y1err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    par1 =  np.polyfit(x,y2,1)
    axs[1].plot(x,pol(x,par1),color="red",linestyle="--",  label=f'pol1 a={round(par1[0],3)}, b={round(par1[1],3)}')

    axs[1].errorbar(x,y2,yerr=y2err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[0].set_xlabel("Layer",size=12)
    axs[0].set_ylabel("Intercept [ns]",size=12)
    axs[0].set_title("TOA Fall",size=14)
    axs[1].set_xlabel("Layer",size=12)
    axs[1].set_ylabel("Intercept [ns]",size=12)
    axs[1].set_title("TOA Rise",size=14)
    axs[0].legend(loc="upper right")
    axs[1].legend(loc="upper right")
    
def plotPol1Parameter(p):
    parameter = 0 # Plotting Slope
    x,y1,y1err,y2,y2err=[],[],[],[],[]
    for layer,_,rise,fall,_,_ in p:
        if layer != 7 and layer != 10 and layer < 19:
            x.append(layer)
            y1.append(fall[0][parameter])
            y1err.append(fall[1][parameter])
            y2.append(rise[0][parameter])
            y2err.append(rise[1][parameter])
            
    
    f, axs = plt.subplots(1,2, figsize = (16,6))
    axs[0].errorbar(x,y1,yerr=y1err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[1].errorbar(x,y2,yerr=y2err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[0].set_xlabel("Layer",size=12)
    axs[0].set_ylabel("Slope",size=12)
    axs[0].set_title("TOA Fall",size=14)
    axs[1].set_xlabel("Layer",size=12)
    axs[1].set_ylabel("Slope",size=12)
    axs[1].set_title("TOA Rise",size=14)
    
    
    
    parameter = 1 # Plotting Intercept
    x,y1,y1err,y2,y2err=[],[],[],[],[]
    for layer,_,rise,fall,_,_ in p:
        if layer != 7 and layer != 10 and layer < 19:
            x.append(layer)
            y1.append(fall[0][parameter])
            y1err.append(fall[1][parameter])
            y2.append(rise[0][parameter])
            y2err.append(rise[1][parameter])
            
    
    f, axs = plt.subplots(1,2, figsize = (16,6))
    axs[0].errorbar(x,y1,yerr=y1err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[1].errorbar(x,y2,yerr=y2err,fmt='o',color='black',
    #                uplims=True, lolims=True
                    capsize=5,
                    ecolor='red'
                   )
    axs[0].set_xlabel("Layer",size=12)
    axs[0].set_ylabel("Intercept [ns]",size=12)
    axs[0].set_title("TOA Fall",size=14)
    axs[1].set_xlabel("Layer",size=12)
    axs[1].set_ylabel("Intercept [ns]",size=12)
    axs[1].set_title("TOA Rise",size=14)