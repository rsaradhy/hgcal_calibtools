import uproot
import numpy as np
import ROOT
import glob
from pprint import pprint
from matplotlib import pyplot as plt
import matplotlib.ticker as ticker
import pandas as pd



def generateTable(dt_collection,luminosity =35.9,till=-1,fig_name = "./table1.png"): #send in a dictionary of uproot files
    yeilds = {}
    total = 0.0
    iter =0
    for channel in dt_collection.keys():
        if iter == till:
            break
        yeilds[channel] = {}
        yeilds[channel]["All"] = np.sum(dt_collection[channel]['weight'])*luminosity
        total+=yeilds[channel]["All"]
        cut = dt_collection[channel]['nEle']==2
        yeilds[channel]["Electron"] = np.sum(dt_collection[channel]['weight'][cut])*luminosity
        cut = dt_collection[channel]['nMuons']==2
        yeilds[channel]["Muon"] = np.sum(dt_collection[channel]['weight'][cut])*luminosity
        iter +=1
    table1 = []
    for channel in yeilds.keys():
        for lepton in sorted(yeilds[channel].iterkeys()):
            table1.append([channel,lepton, round(yeilds[channel][lepton],3),round(yeilds[channel][lepton]*100/total,2)])
    df = pd.DataFrame(table1,columns =['Channel','Lepton','Yield','Yield %'])
    fig, ax = plt.subplots()
    fig.subplots_adjust(top=1,bottom=.99,left=0,right=1) # Only way to get this to work
    # hide axes
    cell_colors=[]
    count = -1
    for i in range(0,df.shape[0]):
        column_color = []
        count+=1
        if count == 0:
            color = 'orange'
        if count == 1:
            color = 'ivory'
        if count == 2:
            color = "lightgray"
            count = -1


        for j in range(0,df.shape[1]):
            column_color.append(color)
        cell_colors.append(column_color)

    colColor=[]
    for j in range(0,df.shape[1]):
        color="wheat"
        colColor.append(color)


    plt.style.use("seaborn-dark-palette")

    plt.Figure(frameon=False)
    ax.table(cellText = df.values,
             colColours= colColor,
             cellColours=cell_colors,
             colLabels=df.columns,
             loc="center",zorder=11)


    ax.set_axis_off()
    ax.xaxis.set_major_locator(ticker.NullLocator())
    ax.yaxis.set_major_locator(ticker.NullLocator())
    fig.savefig(fig_name,dpi=450,bbox_inches='tight',pad_inches=0.0)
    plt.close()



def ApplyIndices(dt,indx):
    output = {}
    for key in dt.keys():
        output[key] = dt[key][indx]
    return output

def ROOT2NP(dt): #Function that converts a Root Tree to a dictionary of NP arrays
    output = {}
    for key in dt.keys():
        output[key] = dt[key].array()
    return output


def getData(rootDir): # all kinds of wildcards can be used
    listOfFiles = glob.glob(rootDir)
    output_data = {}
    for files in listOfFiles:
        fileName = files.split("/")[-1].split("_")[1]+"_"+files.split("/")[-1].split("_")[2]
        nameTree = "ZHLeptonicTagDumper/trees/" + uproot.open(files)["ZHLeptonicTagDumper/trees"].keys()[0].split(';')[0]
        output_data[fileName] = ROOT2NP(uproot.open(files)[nameTree])
    return output_data

def combineDict(dt, channels): #to select and combine channels of interest
    output = {}
    for item in dt[dt.keys()[0]]:
        np_arrays = []
        for channel in channels:
            np_arrays.append(dt[channel][item])
        output[item] = np.concatenate(np_arrays)
    return output


def GetError(dt,dt_wt,bin_edges): #Getting the poisson errors with the correct weights using ROOT
    hist = ROOT.TH1F("test","test",len(bin_edges)-2,bin_edges) #that two is there to fix the later code. *Magic*
    hist.Sumw2();
    for i in range(0,dt.size):
       hist.Fill(dt[i],dt_wt[i])

    error =[]
    for i in range(1,hist.GetSize()):
        error.append(hist.GetBinError(i))
    hist.Delete()
    return error


def cutsParser(arrName,cuts=""):
    string2exec1 = "cut="
    if cuts != "":
        for thecut in cuts.split(';'):
            args = thecut.split(' ')
            if len(args) != 3:
                print "Arguments for cut NOT in format"
                print "Format:: 'VAR OPRT VALUEcond;VAR OPRT VALUE'"
                print "Note the single space in the above line"
                return None
                break

            if thecut != cuts.split(';')[-1]:
                if args[2][-1] != "&" and args[2][-1] != "|":
                    print "unsupported condition operand"
                    return
                string2exec1 += "("+arrName+"['"+args[0]+"']"+args[1]+args[2][:-1]+")"+ args[2][-1]
            else:
                string2exec1 += "("+arrName+"['"+args[0]+"']"+args[1]+args[2]+")"

    else:
        string2exec1 += "(np.ones("+arrName+"[var2plt].size,dtype=bool))"
    return string2exec1


def DrawBkgPlots(real_dat,bkg_dat,var2plt,cuts,xlabel=None,ylabel="Events",sig_data_collection=None,title=None,nbins =30,saveDir="",luminosity=35.9,func=None):
    # Parsing the cuts
    string2exec1,string2exec2 = cutsParser("real_dat",cuts),cutsParser("bkg_dat[0][channel]",cuts)

    # Parsing the cuts Done

    #Plotting Begins
    # Fixing the canvas
    # start with a rectangular Figure
    rect_scatter = [0,.27,1,1] #xmin,ymin,xmax,ymax
    rect_histx = [0,0,1,.25]
    fig = plt.figure(1) #,figsize=(4, 3)

    axMain = plt.axes(rect_scatter)
    axCompare = plt.axes(rect_histx)




    exec(string2exec1) # Executing the string to get cut var


    data2plt1 = real_dat[var2plt][cut]
    wght4data1 = real_dat['weight'][cut]
    counts_Data,bin_edges = np.histogram(
                                data2plt1,
                                bins=nbins)

    # print len(bin_edges), nbins


    bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.

    data_errors = np.array(GetError(data2plt1,wght4data1,bin_edges))
    axMain.errorbar(bin_centres,
                 counts_Data,
                 yerr=data_errors,
                 label = "data",
                 fmt=".",
                 color="black",
                 zorder = 10,
                 capsize=3,
                 elinewidth=1,
                 markeredgewidth=1)

    datalist = []
    wghtlist = []
    for channel in bkg_dat[0].keys():
        exec(string2exec2) # this will get us the cuts
        data2plt2 = bkg_dat[0][channel][var2plt][cut]
        wght4data2 = bkg_dat[0][channel]['weight'][cut]*luminosity
#         print string2exec2
#         print data2plt2.size
#         print wght4data2.size
        #Adding error in Quad
        err_chl = np.array(GetError(data2plt2,wght4data2,bin_edges))
        if channel == bkg_dat[0].keys()[0]:
            error = err_chl*err_chl
        else:
            error += err_chl*err_chl
        #End Quad
        datalist.append(data2plt2)
        wghtlist.append(wght4data2)

    axMain.hist(
            datalist,
            weights = wghtlist, #Applying weights
            edgecolor='black', linewidth=0.5,
            bins = bin_edges,
            alpha = 1,
            label = bkg_dat[1].values(),
            stacked=True
        )
    data2plt0 = np.concatenate(datalist)
    wght4data0 = np.concatenate(wghtlist)
    error = np.sqrt(error)
    #Stat Error
    counts_MC,bin_edges = np.histogram(
                                data2plt0
                                ,bins=bin_edges # make sure it is bin_edges
                                ,weights =wght4data0
                                )
    bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
    axMain.errorbar(
                 bin_centres,
                 counts_MC,
                 yerr=error,
                 label = "Stat. Uncertainty",
                 fmt="none",
                 color="dimgray",
                 alpha = .7,
                 zorder = 11,
                 capsize=3,
                 elinewidth=1,
                 markeredgewidth=1)
    #Making a Textbox for the cuts....
    textstr = ""
    for cut in cuts.split(';'):
        if cut != cuts.split(';')[-1]:
            textstr += cut[:-1] + "\n"
            if cut[-1] == "|":
                textstr += "or\n"
        else:
            textstr += cut


    ax = axMain
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)

    axMain.text(0.35, 0.95,textstr,
             transform=ax.transAxes,
             fontsize=10,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
            label="Cuts")

    # Calculating the total yield
    np.set_printoptions(precision=3)
    textstr = ""
    textstr += "Data Yield: " + str(np.sum(counts_Data)) +"$\pm$" + str(round(np.sqrt(np.sum(data_errors**2)),2))
    textstr += "\n MC Yield: " + str(round(np.sum(counts_MC),0)) +"$\pm$" + str(round(np.sqrt(np.sum(error**2)),2))
    axMain.text(0.03, 0.95,textstr,
         transform=ax.transAxes,
         fontsize=10,
         verticalalignment='top',
         multialignment='center',
         bbox=props,
         zorder=15,
        label="Cuts")


    ###############################
    #     Drawing the signal MC
    if sig_data_collection != None:
        signal = sig_data_collection["ZHToGG_M125"],sig_data_collection["ttHJetToGG_M125"] #Hard code
        sig_cuts = "mass > 115&;mass < 135" #Hard Code
        sig_cuts = "" #Hard Code

        exec(cutsParser("signal[0]",sig_cuts))
        cut1 =cut
        exec(cutsParser("signal[1]",sig_cuts))
        cut2 =cut
        axMain.hist(
                signal[0][var2plt][cut1],
                weights = signal[0]["weight"][cut1]*luminosity*10, #Applying weights
                edgecolor='red', linewidth=1,
                bins = bin_edges,
                histtype = 'step',
                alpha = 1,
                stacked=True,
                color="red",
                label = "ZH 125 [GeV] * 10"
                )
        axMain.hist(
                signal[1][var2plt][cut2],
                weights = signal[1]["weight"][cut2]*luminosity*10, #Applying weights
                edgecolor='darkred', linewidth=1,
                bins = bin_edges,
                histtype = 'step',
                alpha = 1,
                stacked=True,
                color="red",
                label = "TTH 125 [GeV] * 10"
                )

    # End of Drawing the signal MC
    ##############################

#     Adding all the tick marks:
    ax.tick_params(axis='both', left='on', top='off', right='on',
                bottom='on', labelleft='on', labeltop='off',
                labelright='off', labelbottom='on')

#     plt.axis([,150,0,10])

    ## Setting Up CMS Preliminary and luminosity
    ax.text(0, 1.35, r"$\bf{CMS}$ Preliminary",
     transform=fig.transFigure,
     fontsize=18,
     verticalalignment='top',
     multialignment='center',
     # bbox=props,
     # zorder=15,
     label="cms")

    textDisp = str(luminosity) + "fb$^{-1}$ (13TeV)"
    ax.text(0.73, 1.35,textDisp,
      transform=fig.transFigure,
      fontsize=14,
      verticalalignment='top',
      multialignment='center',
      # bbox=props,
      # zorder=15,
      label="lumi")



    ###



    axMain.set_ylim(bottom = 0)
    axMain.legend(loc = "upper right",facecolor='ivory').set_zorder(20)
    # axMain.set_title(title)
    axMain.set_xlabel(None)
    axMain.get_xaxis().set_visible(False)
    axMain.set_ylabel(ylabel)
    axMain.minorticks_on()



    # plotting MC vs Data
    axCompare.set_ylabel("Data / MC")
    axCompare.set_ylim(-1,2)
    axCompare.axhline(y=1, linestyle='dashed')
    axCompare.set_xlabel(xlabel)
    axCompare.set_xlim(axMain.get_xlim())
    axCompare.minorticks_on()
    #Making sure we are selecting only positive values
    data = counts_Data[(counts_MC >0)&(counts_Data >0)]
    monteC =counts_MC[(counts_MC >0)&(counts_Data >0)]
    data_errors = np.array(data_errors)[(counts_MC >0)&(counts_Data >0)]
    mc_error = np.array(error)[(counts_MC >0)&(counts_Data >0)]
    DataByMC = data / monteC
    error = np.sqrt((mc_error/monteC)**2 + (data_errors/data)**2 ) * DataByMC
    axCompare.errorbar(
                 bin_centres[(counts_MC >0)&(counts_Data >0)],
                 DataByMC,
                 yerr=error,
                 label = "Data / MC",
                 fmt="*",
                 color="dimgray",
                 alpha = .7,
                 zorder = 11,
                 capsize=3,
                 elinewidth=1,
                 markeredgewidth=1)
    axCompare.legend(loc="upper right")
    # end of plotting MC vs Data



    #Saving the plot
    if ".png" in saveDir:
        fileName = saveDir
    else:
        fileName = saveDir+title+"_"+var2plt+".png"

    if saveDir !="":
        plt.savefig(fileName,bbox_inches='tight',dpi=300)
        plt.close()
    else:
        plt.show()



    return bin_edges


def applyFormatting(ax,fig):
    ##Formatting
    ax.text(.1, .93, r"$\bf{CMS}$ Preliminary",
         transform=fig.transFigure,
         fontsize=18,
         verticalalignment='top',
         multialignment='center',
         # bbox=props,
         # zorder=15,
         label="cms")

    textDisp = str(35.9) + "fb$^{-1}$ (13TeV)"
    ax.grid()
    ax.minorticks_on()
    ax.tick_params(axis='both', left='True', top='False', right='on',
                    bottom='True', labelleft='True', labeltop='False',
                    labelright='False', labelbottom='True')
    _ = ax.text(0.73, .93,textDisp,
      transform=fig.transFigure,
      fontsize=14,
      verticalalignment='top',
      multialignment='center',
      # bbox=props,
      # zorder=15,
      label="lumi")
