[CHANGE LOG]
--
- Cleaned up ct_pandas
- Added changelog.md
- Lineariser uses cleanSigma to get X-Profile data points instead of | Eng -100 | < 10 cut

---

[TO-DO]
--
- Fixing the energy strip -- causes problems for different energies...
- Save Calibration Figures from each step
- Dropping phase I selection
- TW Fit optimization 
- Error analysis
- Reconstruction Algorithm for better timing.
- Using Both MCP Data to retrive silicon channel timing
