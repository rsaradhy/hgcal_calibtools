import uproot as up
import pandas as pd
import numpy as np

engMap = {
#      50:[922,923,924],
#      80:[971,972,973,974,975],
#     100:[985,987,988,990,991],
#     120:[998,999,1000,1001,1002],
#     150:[926,927,930,932],
#     200:[992,993,994,996,997],
    250:[977,978,979,980],
    300:[918,919,920,965,967,968,969]
} 



#Lets create a function which does this!
def run2DataFrame(run): # function that creates a cleaned data frame of relevant variables.
    data = up.open(f"/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v14/ntuple_{run}.root")
    df_r = data[b"rechitntupler"]["hits"].pandas.df()
    df_m = data['MCPntupler']['MCP'].pandas.df()
    
    #Chaging the Name
    var_list=['run',
              'event',
              "beamEnergy",
              'rechit_layer',
              'rechit_iu',
              'rechit_iv',
              'rechit_z',
              'rechit_x',
              'rechit_y',
              "rechit_energy",
              'rechit_toaFall_time',
              'rechit_toaRise_time'
             ]

    rename_var = {
              'event':'evt', 
              'rechit_iu':'iu',
              'rechit_iv':'iv',
              'rechit_x':'x',
              'rechit_y':'y',
              'rechit_z':'z',
              'rechit_layer':'l',
              "rechit_energy":'e',
              'rechit_toaFall_time':'tf',
              'rechit_toaRise_time':'tr'
    }
    #Building a single file with all good data
    dm = df_m.loc[:,["event","TS_toClock_FE_MCP1",'valid_TS_MCP1']]
    dr = ((df_r).loc[:,var_list]).rename(columns=rename_var)

    
    # Merging MCP data to Rec Data
    value = dm.TS_toClock_FE_MCP1.values
    valid = dm.valid_TS_MCP1
    m=[]
    mv = []
    for i in range(0,dr.index[-1][0]+1):
        size = dr.xs(i).shape[0]
        m.append(np.ones(size)*value[i])
        mv.append(np.ones(size)*valid[i])
    mcp = np.concatenate(m)
    mcp_valid = np.concatenate(mv)
    data = dr.reset_index() 
    data.insert(1, "mcp", mcp, True)
    data.insert(2, "mcp_valid", mcp_valid, True)
#     data=data.set_index(['run','evt','l','iu','iv'],drop=False)
    #print(data.shape)
    data = data.dropna().drop(columns=['entry','subentry']) #Remove every row where atleast one column is nan and drop the entry and subentry columns
    #print(data.shape)
    
    #Clean UP cuts :: Adjust this to reduce the data size
    cleanCut = (data.mcp_valid==1)&(data.tf>0)&(data.tr>0)&(data.e>20)&(data.mcp>0)
    data = data[cleanCut]
    
    return data



#Merging two data sets

def getRuns(runs):
    if type(runs) == type([]):
        dfs=[]
        for run in runs:
            dfs.append(run2DataFrame(run))
            print (f"Run {run} completed")
        dfs = pd.concat(dfs)
        return dfs
    else:
        print("argument should be a list :: remember to put it in '[]' ")


#Save and store in feather format
def save(filename,data):
    data.reset_index().to_feather(f"{filename}.feather")
def load(filename):
    return pd.read_feather(f"{filename}.feather").set_index(['run','evt','l','iu','iv'],drop=False)





if __name__ == "__main__":
    # Collecting all data and saving as FEATHER format for further analysis
    for eng in engMap.keys():
        try:
            dfs = getRuns(engMap[eng])
            save(f"./data/{eng}.GeV",dfs)
            print(f"Save ./data/{eng}.GeV.feather Successful")    
        except:
            print(f"Save ./data/{eng}.GeV.feather Unsuccessful")