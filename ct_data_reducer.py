import uproot as up
import numpy as np
import pandas as pd

def bindMCP(dr,dm): #Make MCP data size same as silicon data, and concatenate
    data = dr.reset_index()
    data = data.dropna().drop(columns=['entry','subentry'])
    repeat_values = dr.groupby('evt').run.count().values
    for key in dm.columns:
        data[key] = np.repeat(dm[key],repeat_values).values
    
    data.set_index(['run','evt','l','iu','iv'],drop=False)
    return data


#Lets create a function which does this!
def run2DataFrame(run): # function that creates a cleaned data frame of relevant variables.
    data = up.open(f"/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v14/ntuple_{run}.root")
    df_r = data[b"rechitntupler"]["hits"].pandas.df()
    df_m = data['MCPntupler']['MCP'].pandas.df()
    #creating dataframe compatible with TW Calibrater
    #Chaging the Name
    var_list=['run',
              'event',
              "beamEnergy",
              "rechit_chip",
              'rechit_channel',
              'rechit_layer',
              'rechit_iu',
              'rechit_iv',
              'rechit_z',
              'rechit_x',
              'rechit_y',
              
              'rechit_energy',
              'rechit_hg_goodFit',
              'rechit_lg_goodFit',
              'rechit_hg_saturated',
              'rechit_lg_saturated',
              'rechit_noise_flag',
              
              'rechit_toa_calib_flag',
              'rechit_toaFall_flag',
              'rechit_toaRise_flag',
              
              'rechit_toaFall',# Not linearized raw data
              'rechit_toaRise',# Not linearized raw data
              'rechit_toaFall_time',
              'rechit_toaRise_time'
             ]

    rename_var = {
              'event':'evt', 
              'rechit_chip':'chip',
              'rechit_channel':'channel',
              'rechit_iu':'iu',
              'rechit_iv':'iv',
              'rechit_x':'x',
              'rechit_y':'y',
              'rechit_z':'z',
              'rechit_layer':'l',
              "rechit_energy":'e',
              'rechit_toaFall_time':'tf',
              'rechit_toaRise_time':'tr'
    }
    dr = ((df_r).loc[:,var_list]).rename(columns=rename_var)
   

    #Doing the same thing for MCP data
    mcp_vars = ["event",
                "TS_toClock_FE_MCP1",
                'valid_TS_MCP1',
                'amp_MCP1',
                "TS_toClock_FE_MCP2",
                'valid_TS_MCP2',
                'amp_MCP2'
               ]

    mcp_rename = {
                "TS_toClock_FE_MCP1":"mcp_1",
                'valid_TS_MCP1':"mcp_valid_1",
                'amp_MCP1':"mcp_ampl_1",
                "TS_toClock_FE_MCP2":"mcp_2",
                'valid_TS_MCP2':"mcp_valid_2",
                'amp_MCP2':"mcp_ampl_2"}


    dm = (df_m.loc[:,mcp_vars]).rename(columns=mcp_rename)

    df = bindMCP(dr,dm)
    
    theCut = (df.e>20)&(df.tf>0)&(df.tr>0)&(df.mcp_1>0)&(df.mcp_2>0)&(df.mcp_ampl_1>0)&(df.mcp_ampl_2>0)
    df = df[theCut]
    
    return df


    