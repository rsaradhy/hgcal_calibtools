import uproot as up
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats as st
import pandas as pd
from matplotlib.colors import LogNorm
from matplotlib.gridspec import GridSpec

import matplotlib as mpl
mpl.rcParams['image.cmap'] = 'inferno'
from scipy.optimize import curve_fit
from scipy.stats import iqr, norm, binned_statistic, binned_statistic_2d
from scipy.special import erf
from . import  calib_results as cr
from . import ct_pandas_plots as cplts
import scipy.optimize as opt
import pickle




## Global Variables
ENERGY_CUT = 50
ENERGY_WIDTH = 1000
BINS_Fast=150
BINS_Slow=500
BINS_Disp=250
fit_range=[0,12.5]
# fit_range=[0,24]
pol_order = 3



def setParameters(Layer=9, iu=-2, iv=1, BE=300,set_lin_pars='111'):
    cr.parameters_set = 1
    cr.layer = Layer
    cr.u =iu
    cr.v =iv
    cr.BE =BE
    cr.set_lin_pars = str(set_lin_pars)
    # cr.print_vars(7)

def save_pkl(obj, name ):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_pkl(name ):
    with open(name, 'rb') as f:
        return pickle.load(f)

def save_feather(filename,data):
    data.reset_index(drop=True).to_feather(f"{filename}.feather")

def load_feather(filename):
    return pd.read_feather(f"{filename}.feather").set_index(['run','evt','l','iu','iv'],drop=False)

def quickClean(df):
    upper = 26
    lower = -26
    eng = 20
    sel = ((df.tr)<= upper)&((df.tr)>= lower)&((df.tf)<= upper)&((df.tf)>= lower)&(df.e>eng)
    sel=(sel)&(df.mcp_1>=lower)&(df.mcp_1<=upper)&(df.mcp_valid_1==1)&(df.mcp_2>=lower)&(df.mcp_2<=upper)&(df.mcp_valid_2==1)
    #&(df.mcp>=lower)&(df.mcp<=upper)&(df.mcp_valid==1)
    return df[sel]

def get_version():
    version = 2.5
    print(f"Calib Toolkit (based on Pandas) Version {version}")




def mergePop(toa,mcp,energy,edge="tf",debug=1):
    offset =2
   
    if edge=="tr":
        label="TOA_Rise"
        upper = (1,5.5+offset)
        upper_label = f"y=x+{upper[1]}"
        lower = (1,-18.5+offset)
        lower_label = f"y=x {lower[1]}"
        loc="upper left"
        title="RISE"
        push=0
    elif edge=="tf":
        label="TOA_Fall"
        upper = (1,17.5+offset)
        upper_label = f"y=x+{upper[1]}"
        lower = (1,-7.5+offset)
        lower_label = f"y=x {lower[1]}"
        loc="upper right"
        title="FALL"
        push=12.5
    # if edge=="tf":
    #     label="TOA_Fall"
    #     upper = (1,5.5)
    #     upper_label = "y=x+5.5"
    #     lower = (1,-18.5)
    #     lower_label = "y=x-18.5"
    #     loc="upper left"
    #     title="FALL"
    #     push=0
    # elif edge=="tr":
    #     label="TOA_Rise"
    #     upper = (1,17.5)
    #     upper_label = "y=x+18"
    #     lower = (1,-7.5)
    #     lower_label = "y=x-7.5"
    #     loc="upper right"
    #     title="RISE"
    #     push=12.5


    # if 1:
    if debug:
        f, ax = plt.subplots( figsize = (8,6))
        xx=np.linspace(0,25,100)
        _=ax.hist2d(mcp,toa,bins=BINS_Fast,norm=LogNorm())
        _=f.colorbar(_[3], ax=ax)
        _=ax.plot(xx,pol(xx,upper),linewidth=5,label=upper_label)
        _=ax.plot(xx,pol(xx,lower),linewidth=5,label=lower_label)
        # _=ax.axhline(0)
        _=ax.set_xlabel("MCP [ns]",fontsize=14)
        _=ax.set_ylabel("TOA [ns]",fontsize=14)
        _=ax.legend(loc=loc)
        # _=ax.set_title(title)
        _=ax.set_ylim(0,26)
        _=ax.set_xlim(0,26)


        applyFormatting(f,ax)

        if cr.save_fig:
            f.savefig(f'./figure/{cr.layer_name}_{edge}_00_toamcp_raw.png')

        
    
    # algo 1        
    toa_fixed = toa + (toa<lower[0]*mcp + lower[1])*25
    toa_fixed -=  (toa>upper[0]*mcp + upper[1])*25 
    # toa_fixed += 12.5
     # algo 2



    # if debug:
    #     f, ax = plt.subplots( figsize = (8,6))
    #     xx=np.linspace(0,25,60)
    #     _=ax.hist2d(mcp,toa_fixed,bins=BINS_Fast,norm=LogNorm())
    #     _=f.colorbar(_[3], ax=ax)
    #     # _=ax.plot(xx,pol(xx,upper),linewidth=5,label=upper_label)
    #     # _=ax.plot(xx,pol(xx,lower),linewidth=5,label=lower_label)
    #     _=ax.set_xlabel("MCP [ns]",fontsize=14)
    #     _=ax.set_ylabel("Merged TOA [ns]",fontsize=14)
    #     # _=ax.legend(loc=loc)
    #     _=ax.set_title(title)
    #     if 'layer_name' in globals():
    #         print("saving TOA FALL vs MCP")
    #         f.savefig(f'./figure/TOA_Merged_toaFall_{layer_name}.png')

    
    
    # algo 2        
    # toa_fixed = toa + ((toa<upper[0]*mcp + upper[1])&(toa>lower[0]*mcp + lower[1]))*25
    # toa_fixed +=  (toa<lower[0]*mcp + lower[1])*50
    
    # algo 2 fix MCP
    if(edge=='tf'):
        mcp_fixed = mcp + ((toa<upper[0]*mcp + upper[1])&(toa>lower[0]*mcp + lower[1]))*12.5
        mcp_fixed += (toa>upper[0]*mcp + upper[1])*37.5
        mcp_fixed -=  (toa<lower[0]*mcp + lower[1])*12.5
    elif (edge=='tr'):
        mcp_fixed = mcp +  (toa>upper[0]*mcp + upper[1])*25
        # ((toa<upper[0]*mcp + upper[1])&(toa>lower[0]*mcp + lower[1]))*12.5
        mcp_fixed -=  (toa<lower[0]*mcp + lower[1])*25





    
    if debug:
        f, ax = plt.subplots( figsize = (8,6))
        xx=np.linspace(np.min(mcp_fixed),np.max(mcp_fixed),100)
        _=ax.hist2d(mcp_fixed,toa,bins=BINS_Fast,norm=LogNorm())
        _=f.colorbar(_[3], ax=ax)
        # _=ax.plot(xx,pol(xx,upper),linewidth=5,label=upper_label)
        # _=ax.plot(xx,pol(xx,lower),linewidth=5,label=lower_label)
        # _=ax.axhline(0)
        _=ax.set_xlabel("Merged MCP [ns]",fontsize=14)
        _=ax.set_ylabel("TOA [ns]",fontsize=14)
        _=ax.set_ylim(0,None)
        # _=ax.set_ylim(0,25)

        # _=ax.legend(loc=loc)
        # _=ax.set_title(title)
        applyFormatting(f,ax)

        if cr.save_fig:
            print(f"saving {label} vs MCP")
            f.savefig(f'./figure/{cr.layer_name}_{edge}_01_toamcp_merged_mcp.png')
            

    # toa_upper = toa[(toa>upper[0]*mcp + upper[1])]
    toa_middle = toa[(toa>lower[0]*mcp + lower[1])&(toa<=upper[0]*mcp + upper[1])  ]
    toa_lower = toa[(toa<=lower[0]*mcp + lower[1])]
    





    # We fit it and correct the intercept to be zero
    data_cut = (toa>0)&(toa<12.5)
    # energy_cut = (np.abs(energy - 300)<ENERGY_WIDTH)


    

    energy_cut = (np.abs(energy - ENERGY_CUT)<ENERGY_WIDTH)
    theCut = data_cut&energy_cut
    w_toa = toa[theCut]
    w_mcp = mcp_fixed[theCut]



    bins=100
    (x,y),_,_ = binned_statistic(w_toa, [w_toa,w_mcp], bins=bins, statistic="median")

    theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
    xx = x[theCut]
    yy = y[theCut]

    # Note that this section is required if we were to take the X-Profile
    # theCut = (xx<10)&(xx>2)&(yy>2)
    # xx = xx[theCut]
    # yy = yy[theCut]


    par1 = np.polyfit(xx,yy,1)

    
    #Plotting MCP MERGED AND SHIFTED
    if debug:
        f, ax = plt.subplots( figsize = (8,6))
        _=ax.hist2d(mcp_fixed,toa,bins=BINS_Fast,norm=LogNorm())
        _=f.colorbar(_[3], ax=ax)
        # _=ax.hist2d(w_mcp,w_toa,bins=BINS_Fast,norm=LogNorm())
        _=ax.scatter(yy,xx,label=r"X-Profile TOA$\in$[2,10]")
        _=ax.plot(pol(xx,par1),xx,color="red",linestyle="--",label=get_labels(par1),linewidth=3,zorder=12)
        _=ax.set_ylim(0,25)
        # _=ax.set_title("Before Fixing Offset")
        _=ax.set_xlabel("Merged MCP [ns]",fontsize=14)
        _=ax.set_ylabel("TOA [ns]",fontsize=14)
        _=ax.legend(loc="upper left")
        applyFormatting(f,ax)
        if cr.save_fig:
            f.savefig(f'./figure/{cr.layer_name}_{edge}_01_toamcp_offset_fixing_b.png')
        
        f, ax = plt.subplots( figsize = (8,6))
        _=ax.hist2d(mcp_fixed-par1[1],toa,bins=BINS_Fast,norm=LogNorm())
        _=f.colorbar(_[3], ax=ax)
        _=ax.scatter(yy-par1[1],xx,label=r"X-Profile TOA$\in$[2,10]")
        _=ax.plot(pol(xx,par1)-par1[1],xx,color="red",linestyle="--",label=get_labels((par1[0],0)),linewidth=3,zorder=12)
        _=ax.set_ylim(0,25)
        # _=ax.set_title("After Fixing Offset")
        _=ax.set_xlabel("Merged MCP [ns]",fontsize=14)
        _=ax.set_ylabel("TOA [ns]",fontsize=14)
        _=ax.legend(loc="upper left")
        applyFormatting(f,ax)

        if cr.save_fig:
            f.savefig(f'./figure/{cr.layer_name}_{edge}_02_toamcp_offset_fixing_a.png')

    mcp_fixed = mcp_fixed-par1[1]# This is very important for linearisation


    #quick plot for showcasing the effect of transformation
    if 0:
        energy_cut = (np.abs(energy - ENERGY_CUT)<ENERGY_WIDTH)
        theCut = energy_cut#data_cut
        w_toa = toa[theCut]
        w_mcp = mcp_fixed[theCut]
        bins=100
        (x,y),_,_ = binned_statistic(w_toa, [w_toa,w_mcp], bins=bins, statistic="median")
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        xx = x[theCut]
        yy = y[theCut]

        f, ax = plt.subplots( figsize = (8,6))
        ax.scatter(25-y,25-x,label="Without Transformation",linewidth=3)
        ax.scatter(y,x,label="With Transformation",linewidth=3)
        x=np.linspace(0,25,100)
        ax.plot(x,pol(x,(1,0)),label="Y=X+0",
        linewidth=4,linestyle='--',color='r')

        ax.set_ylim(0,25)
        ax.set_xlim(0,25)
        ax.grid(which='both')
        ax.legend(loc='upper left')
        _=ax.set_xlabel("Merged MCP [ns]",fontsize=14)
        _=ax.set_ylabel("TOA [ns]",fontsize=14)




    # if debug:
    #     f, ax = plt.subplots( figsize = (8,6))
    #     _=ax.hist2d(mcp_fixed[np.abs(energy-400)<10],toa[np.abs(energy-400)<10],bins=BINS_Fast,norm=LogNorm())
    #     _=f.colorbar(_[3], ax=ax)
    #     _=ax.set_title("|E-400|<10")
    #     _=ax.set_xlabel("Merged MCP [ns]",fontsize=14)
    #     _=ax.set_ylabel("TOA [ns]",fontsize=14)




    #recording to the global variable
 
    # print("This is ",cr.g_mergePop_offset)
    cr.mergePop_offset = par1[1]
    # print("This is ",cr.g_mergePop_offset)


    return toa_fixed,mcp_fixed #toa_fixed is not used for eval






    

def lineariser(toa,mcp,eng,doIt=True,debug=0,correctSlope=1,correctIntercept=1,label=""):
    toa = toa.values
    mcp = mcp.values
    eng = eng.values    

    # print(doIt, correctSlope, correctIntercept)
    data_cut = (toa>0)&(toa<25)#&(mcp>0)&(mcp<25) #Very Important for deriving across channels and layers
    
    energy_cut = (np.abs(eng - ENERGY_CUT)<ENERGY_WIDTH)#(eng>ENERGY_CUT)&(eng<ENERGY_CUT + ENERGY_WIDTH)

      # Clean Up Part...
    # clean_toa  = toa - pol(mcp,(1,0))
    # f, ax = plt.subplots( figsize = (8,6))
    # ax.hist2d(mcp,clean_toa,bins,norm=LogNorm()) 
    sigmaCut = cleanSigma(toa-mcp,0.1)
    

  
    mainCut=energy_cut&data_cut&sigmaCut
    w_eng = eng[(mainCut)]
    w_toa = toa[(mainCut)]
    w_mcp = mcp[(mainCut)]
    

    data={}
    
    
    bins=BINS_Fast
    
    # (x,y),_,_ = binned_statistic(w_mcp, [w_mcp,w_toa], bins=bins, statistic="median")
    (y,x),_,_ = binned_statistic(w_toa, [w_toa,w_mcp], bins=bins, statistic="mean") # Y-Profile --takes care of edge effects
    theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
    xx = x[theCut]
    yy = y[theCut]

    # print(fit_range[0])
    theCut=(yy>fit_range[0])&(yy<fit_range[1])
    x = xx[theCut]
    y = yy[theCut]

    
    #cleaning the edge effects...
    theCut=(y>fit_range[0])&(y<fit_range[1])
    x = x[theCut]
    y = y[theCut]

    theCut=(yy>np.max(y))&(yy<np.max(yy))
    xx = xx[theCut]
    yy = yy[theCut]




    par1 = np.polyfit(x,y,1) #correct one
    par2 = np.polyfit(xx,yy,pol_order) #correction
    
    #Adding it to CR vars 
    cr.lin_linear_par_1 = par1
    cr.lin_poly_par_1 =par2


    #Plotting Non-Linearity
    if debug:
        f, ax = plt.subplots(figsize = (8,6))
        gs = GridSpec(2, 2, width_ratios=[20, 1], height_ratios=[5, 1])

        ax = plt.subplot(gs[0,0])
        _=ax.hist2d(w_mcp,w_toa,bins=bins,norm=LogNorm())
        # _=ax.hist2d(mcp,toa,bins=bins,norm=LogNorm())
        _=f.colorbar(_[3], cax=plt.subplot(gs[:,1]))
        applyFormatting(f,ax)

        ax.set_ylabel("TOA [ns]",fontsize=14)
        ax.scatter(x,y,color="blue",label="Linear Data [X-Profile]",linewidth=1,zorder=10)
        ax.scatter(xx,yy,color="black",label="Non-Linear Data [X-Profile]",linewidth=1,zorder=9)

        X = np.linspace(0,25,50)
        ax.plot(X,pol(X,par1),color="red",linestyle="--",label=get_labels(par1),linewidth=3,zorder=12)
        ax.plot(X,pol(X,par2),color="green",linestyle="--",label=get_labels(par2),linewidth=3,zorder=11)
        ax.legend(loc="upper left")
        # ax.set_title(f"{label}:Before Correction")
        xlim=ax.get_xlim()

        ax = plt.subplot(gs[1,0])
        ax.set_xlim(xlim)
        X=np.concatenate((x,xx))
        Y=np.concatenate((y,yy))
        res = (Y-pol(X,par1))
        ax.bar(X,res)
        ax.set_ylabel(r"$\Delta Y$ [ns]",fontsize=14)
        ax.set_xlabel("MCP [ns]",fontsize=14)
        applyFormatting(f,ax)

        if cr.save_fig:
            f.savefig(f'./figure/{cr.layer_name}_{label}_02_toamcp_raw.png')

        plotTW(eng,toa,mcp,title=f"{label}_02_tw_raw")

        

    #Non-Linear Correction    
    if doIt:
        # if debug:
        #     plotTW(eng,toa,mcp,title=f"{label}:TIME WALK before Non-Linear correction")

        # toa -= (pol(mcp,par2)-pol(mcp,par1)) 
        toa[(mcp>fit_range[1])] -= (pol(mcp[mcp>fit_range[1]],par2)-pol(mcp[mcp>fit_range[1]],par1)) 


        # mcp -= (pol(mcp,par2)-pol(mcp,par1))

        w_toa = toa[mainCut]
        w_mcp = mcp[mainCut]
        # (x,y),_,_ = binned_statistic(w_mcp, [w_mcp,w_toa], bins=bins, statistic="median") #X-Profile
        (y,x),_,_ = binned_statistic(w_toa, [w_toa,w_mcp], bins=bins, statistic="mean") # Y-Profile --takes care of edge effects
        
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        xx = x[theCut]
        yy = y[theCut]

        # print(fit_range[0])
        theCut=(yy>fit_range[0])&(yy<fit_range[1])
        x = xx[theCut]
        y = yy[theCut]


        #cleaning the edge effects...
        theCut=(y>fit_range[0])&(y<fit_range[1])
        x = x[theCut]
        y = y[theCut]

        # theCut=(yy>np.min(yy)+1)&(yy<np.max(yy)-1)
        theCut=(yy>np.max(y))&(yy<25)

        xx = xx[theCut]
        yy = yy[theCut]



        #Fitting the linear part for comparison although this is quite redundant since we are fitting within interval that we are not changing ...
        par1,par1_cov = np.polyfit(x,y,1,cov=True)
        cr.lin_linear_par_2 = (par1,np.sqrt(np.diag(par1_cov)))
      

        # print (np.sqrt(np.diag(par1_cov)))
        
        par2,par2_cov = np.polyfit(xx,yy,pol_order,cov=True)
        cr.lin_poly_par_2 = (par2,np.sqrt(np.diag(par2_cov)))
        

        #Plotting Linearization
        if debug:
            f, ax = plt.subplots( figsize = (8,6))
            gs = GridSpec(2, 2, width_ratios=[20, 1], height_ratios=[5, 1])
            ax = plt.subplot(gs[0,0])
            _=ax.hist2d(mcp,toa,bins=bins,norm=LogNorm())
            _=f.colorbar(_[3], cax=plt.subplot(gs[:,1]))

            ax.set_ylabel("TOA [ns]")
            ax.scatter(xx,yy,color="black",label="Non-Linear Data [X-Profile]",linewidth=1)
            ax.scatter(x,y,color="blue",label="Linear Data [X-Profile]",linewidth=1)
            X = np.linspace(0,25,50)

            ax.plot(X,pol(X,par1),color="red",linestyle="--", label=get_labels(par1),linewidth=3)
            ax.plot(xx,pol(xx,par2),color="green",linestyle="--",label=get_labels(par2),linewidth=3)
            ax.legend(loc="upper left")
            # ax.set_title(f"{label}:After Correction to pol1")
            xlim=ax.get_xlim()
            applyFormatting(f,ax)

            ax = plt.subplot(gs[1,0])
            X=np.concatenate((x,xx))
            Y=np.concatenate((y,yy))
            res = (Y-pol(X,par1))
            ax.bar(X,res)
            ax.set_xlim(xlim)
            ax.set_ylabel(r"$\Delta$Y [ns]",fontsize=14)
            ax.set_xlabel("MCP [ns]",fontsize=14)
            if cr.save_fig:
                f.savefig(f'./figure/{cr.layer_name}_{label}_03_toamcp_nonlinearcorrection_a.png')            
            # #Plotting Time Walk
            plotTW(eng,toa,mcp,title=f"{label}_03_tw_nonlinearcorrection_a")

        
    
    # Slope Correction
    if correctSlope:
        avg_mcp = np.mean(mcp)
        # print(avg_mcp)
        toa -= (pol(mcp-avg_mcp,par1) - pol(mcp-avg_mcp,(1,par1[1])))
        # toa -= (pol(mcp,par1) - pol(mcp,(1,par1[1])))
        w_toa = toa[mainCut]
        w_mcp = mcp[mainCut]
        (x,y),_,_ = binned_statistic(w_mcp, [w_mcp,w_toa], bins=bins, statistic="median")
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        x = x[theCut]
        y = y[theCut]

        #Cleaning the edge effects
        theCut=(y>np.min(y))&(y<np.max(y))
        x = x[theCut]
        y = y[theCut]
               
        par1,par1_cov = np.polyfit(x,y,1,cov=True)
        cr.lin_linear_par_3 = (par1,np.sqrt(np.diag(par1_cov)))
        
        par2 = np.polyfit(x,y,pol_order)
        cr.lin_poly_par_3 = (par2,np.sqrt(np.diag(par2_cov)))


        #Plotting Slope Correction
        if debug:    

            f, ax = plt.subplots( figsize = (8,6))
            _=ax.hist2d(mcp,toa,bins=bins,norm=LogNorm())
            _=f.colorbar(_[3], ax=ax)

            ax.set_xlabel("MCP [ns]",fontsize=14)
            ax.set_ylabel("TOA [ns]",fontsize=14)
            ax.scatter(x,y,color="blue",label="X-Profile")

            ax.plot(x,pol(x,par1),color="red",linestyle="--",label=get_labels(par1),linewidth=3)
            ax.plot(x,pol(x,par2),color="green",linestyle="--",label=get_labels(par2),linewidth=3)
            ax.legend(loc="upper left")
            # ax.set_title(f"{label}:After Correction of Slope")
            applyFormatting(f,ax)
            if cr.save_fig:
                f.savefig(f'./figure/{cr.layer_name}_{label}_04_toamcp_slopecorrection_a.png')

            plotTW(eng,toa,mcp,title=f"{label}_04_tw_slopecorrection_a")
            


    #Intercept Correction 
    if correctIntercept:
        #Correcting Intercept
        toa -= par1[1]
        w_toa = toa[mainCut]
        w_mcp = mcp[mainCut]
        # (x,y),_,_ = binned_statistic(w_mcp, [w_mcp,w_toa], bins=bins, statistic="median")
        (y,x),_,_ = binned_statistic(w_toa, [w_toa,w_mcp], bins=bins, statistic="mean") # Y-Profile --takes care of edge effects

        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        x = x[theCut]
        y = y[theCut]
        #Cleaning the edge effects
        theCut=(y>np.min(y))&(y<np.max(y))
        x = x[theCut]
        y = y[theCut]
        par1 = np.polyfit(x,y,1)
        par2 = np.polyfit(x,y,2)

        #Plotting Intercept Correction        
        if debug:        
            f, ax = plt.subplots( figsize = (8,6))
            _=ax.hist2d(mcp,toa,bins=bins,norm=LogNorm())
            _=f.colorbar(_[3], ax=ax)
            ax.set_xlabel("MCP [ns]",fontsize=14)
            ax.set_ylabel("TOA [ns]",fontsize=14)
            ax.scatter(x,y,color="blue",label="X-Profile")
            ax.plot(x,pol(x,par1),color="red",linestyle="--",  label=get_labels(par1),linewidth=3)
            ax.plot(x,pol(x,par2),color="green",linestyle="--",label=get_labels(par2),linewidth=3)
            # ax.plot(x,pol(x,par3),color="black",linestyle="--",label=f'pol3 a={round(par3[0],3)}, b={round(par3[1],3)}, c={round(par3[2],3)}, d={round(par3[3],3)}')
            ax.legend(loc="upper left")
            # ax.set_title(f"{label}:After of Intercept Correction")
            applyFormatting(f,ax)
            if cr.save_fig:
                f.savefig(f'./figure/{cr.layer_name}_{label}_05_toamcp_interceptcorrection_a.png')

            #Plotting Time Walk
            plotTW(eng,toa,mcp,title=f"{label}_05_tw_interceptcorrection_a")


        
        
        

    # Clean Up Part...
    clean_toa  = toa - pol(mcp,(1,0))
    cut = cleanSigma(clean_toa,4)

        
    data['toa'] = toa
    data['mcp'] = mcp
    data['eng'] = eng
    data['cut'] = cut

    return data






def fixTOA(df_original,debug=0):
    set_lin_pars = cr.set_lin_pars  
 
    if(cr.BE == "0"):
        sel = (df_original.l==cr.layer)&(df_original.iu==cr.u)&(df_original.iv==cr.v)
    else:
        sel = (df_original.l==cr.layer)&(df_original.iu==cr.u)&(df_original.iv==cr.v)&(df_original.beamEnergy==cr.BE)

    df=(df_original.copy())[sel]

    df.tf = 25-df.tf
    df.tr = 25-df.tr
    df.mcp = 25-df.mcp

    # TOA FALL    
    tf_merged,mcp_merged = mergePop(df.tf,df.mcp,df.e,edge='tf',debug=debug)

    # Saving these to the data_frame
    df["tf_mcp_merged"] = mcp_merged
    df["tf_merged"] = tf_merged
    
    data=lineariser(df.tf,df.tf_mcp_merged,df.e,doIt=int(set_lin_pars[0]),debug=debug,correctSlope=int(set_lin_pars[1]),correctIntercept=int(set_lin_pars[2]),label="tf")
    df['tf_f']=data['toa']
    df['tf_mcp_f']=data['mcp']
    df['tf_cut']=data['cut']

    # END OF TOA FALL    


    # TOA RISE    
    tr_merged,mcp_merged = mergePop(df.tr,df.mcp,df.e,edge='tr',debug=debug)
    # Saving these to the data_frame
    df["tr_mcp_merged"] = mcp_merged
    df["tr_merged"] = tr_merged

    data=lineariser(df.tr,df.tr_mcp_merged,df.e,doIt=int(set_lin_pars[0]),debug=debug,correctSlope=int(set_lin_pars[1]),correctIntercept=int(set_lin_pars[2]),label="tr")

    df['tr_f']=data['toa']
    df['tr_mcp_f']=data['mcp']
    df['tr_cut']=data['cut']

    return df


def tw(x,a,b,c,d):
    return a*x + b + np.sqrt(c / (x - d))
    # return a*x + b + c / (x - d)


def fit(X1,Y1,X2,Y2,Layer,bins,debug=0):

    # Creating all axes and figures
    if cr.debug:
        f, ax = plt.subplots(1,1, figsize = (16,7))
        gs = GridSpec(2, 2, width_ratios=[1, 1], height_ratios=[6, 1])
        
        # defining the grid
        ax11 = plt.subplot(gs[0,0])
        ax12 = plt.subplot(gs[0,1])
        ax21 = plt.subplot(gs[1,0])
        ax22 = plt.subplot(gs[1,1])


        # ax = ax11
        # ax.hist2d(X1,Y1,bins,norm=LogNorm())
        # ax = ax12
        # ax.hist2d(X2,Y2,bins,norm=LogNorm())

    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)


    #Binning and Taking X-Profile

    # [TOA-FALL]
    # bins=50
    bins = np.logspace(np.log10(X1.min()),np.log10(X1.max()), 20)  
    (x11,toa_fall),_,_ = binned_statistic(X1, [X1,Y1], bins=bins, statistic="median")
    (_,toa_fall_err),_,_ = binned_statistic(X1, [X1,Y1], bins=bins, statistic=np.std)
    (_,x11_stat),_,_ = binned_statistic(X1, [X1,Y1], bins=bins, statistic="count")

    #Cleaning Up the binned stats
    theCut = np.invert(np.isnan(x11))&np.invert(np.isnan(toa_fall))&(x11_stat>25)
    x1 = x11[theCut]
    toa_fall = toa_fall[theCut]
    #Fix this!!!!!
    toa_fall_err = toa_fall_err[theCut] #(grossly overestimates without the 15)
    x11_stat = x11_stat[theCut]
    # END OF [TOA-FALL]


    # [TOA-RISE]

    (x22,toa_rise),_,_= binned_statistic(X2, [X2,Y2], bins=bins, statistic='median')
    (_,x22_stat),_,_= binned_statistic(X2, [X2,Y2], bins=bins, statistic='count')
    (_,toa_rise_err),_,_ = binned_statistic(X2, [X2,Y2], bins=bins, statistic=np.std)


    theCut = np.invert(np.isnan(x22))&np.invert(np.isnan(toa_rise))&(x22_stat>25)
    x2 = x22[theCut]
    toa_rise = toa_rise[theCut]
    #Fix this!!!!!
    toa_rise_err = toa_rise_err[theCut]  #(grossly overestimates without the 15)
    x22_stat = x22_stat[theCut]
    # END OF [TOA-RISE]




    #Fitting using opt.least_squares
    residual=lambda par,x,y,yErr: (tw(x,*par)-y)/(yErr)
    initPar = (0.001,3,320,0)




    #Fall Fit
    # fall_err = (1/x1/np.sqrt(x11_stat+0.0000000001))
    # fall_err = (1/x1)

    fall_fit = opt.least_squares(residual,initPar,args=(x1,toa_fall,toa_fall_err))
    popt_fall = fall_fit.x
    J=fall_fit.jac
    pcov_fall = np.linalg.inv(J.T.dot(J))
    cr.tw_fit_fall = fall_fit
     

    #Rise Fit
    # toa_rise_err = (1/x2/np.sqrt(x22_stat+0.0000000001))
    rise_fit = opt.least_squares(residual,initPar,args=(x2,toa_rise,toa_rise_err))
    popt_rise = rise_fit.x
    J=rise_fit.jac
    pcov_rise = np.linalg.inv(J.T.dot(J))
    cr.tw_fit_rise = rise_fit


    parameter_name = ['a','b','c','d']
    textstr = r"$f(x)=ax +b +\sqrt{\frac{c}{x-d}}$"+"\n"
    # textstr = r"$f(x)=ax +b +\frac{c}{x-d}$"+"\n"
    

    fall_err = np.sqrt(np.diag(pcov_fall))
    rise_err = np.sqrt(np.diag(pcov_rise))
    

    # Plotting the fits
    if cr.debug:
        ax = ax11
        # r0 = ax.scatter(x1,toa_fall,marker="*",label="X-Profile")
        ax.hist2d(X1,Y1,bins = 250, norm=LogNorm())
        ax.errorbar(x1,toa_fall,yerr=toa_fall_err,fmt='o',color='blue',
                        capsize=5,
                        ecolor='green'
                        ,label=f'X-Profile TOA < {fit_range[1]}'
                       )


        ax.set_ylabel("MCP - TOA_Fall (ns)")
        ax.set_xlabel("Energy (MIPs)")
        ax.plot(
                    x1, tw(x1,*popt_fall)
                    ,color='red'
                    ,linewidth='3'
                    ,linestyle='--'
                    ,label="Fit"
                    ,zorder=11
        )

        # ax.set_title(f"Layer = {Layer}; TOA < {fit_range[1]}")
        ax.legend(loc="upper right")
        applyFormatting(f,ax)
        #Residuals
        ax21.plot(x1,fall_fit.fun)
        ax21.set_ylabel("Weight. Res")
        ax21.set_xlim(ax.get_xlim())



        
      
        
        textstr_app = textstr
        for i,j,k in zip(popt_fall,parameter_name,fall_err):
            textstr_app+=f" {j} = {round(i,3)}"+r"$\pm$"+f"{round(k,3)}\n"
        
        textstr_app+=r"$\chi^{2}_{\nu}$"+f" = {round(chi2(fall_fit),3)}"
        
                
        ax.text(0.5, 0.85,textstr_app,
            transform=ax.transAxes,
            fontsize=10,
            verticalalignment='top',
            multialignment='center',
            bbox=props,
            label="Cuts")
        


        



        #Second Graph
        ax = ax12
        ax.hist2d(X2,Y2,bins = 250, norm=LogNorm())

        ax.set_title(f"Layer = {Layer}; TOA < {fit_range[1]}")
        # r1 = ax.scatter(x2,toa_rise,marker="*",label="X-Profile")
        ax.errorbar(x2,toa_rise,yerr=toa_rise_err,fmt='o',color='blue',
                        capsize=5,
                        ecolor='green'
                        ,label=f'X-Profile TOA < {fit_range[1]}'
                       )

        ax.set_ylabel("MCP - TOA_Rise (ns)")
        ax.set_xlabel("Energy (MIPs)")
        ax.plot(
                    x2, tw(x2,*popt_rise)
                    ,color='red'
                    ,linewidth='3'
                    ,linestyle='--'
                    ,label="Fit"
                    ,zorder=11

        )
        
        
        textstr_app = textstr
        for i,j,k in zip(popt_rise,parameter_name,rise_err):
            textstr_app+=f" {j} = {round(i,3)}"+r"$\pm$"+f"{round(k,3)}\n"
        textstr_app+=r"$\chi^{2}_{\nu}$"+f" = {round(chi2(rise_fit),3)}"       
        
        ax.text(0.5, 0.85,textstr_app,
            transform=ax.transAxes,
            fontsize=10,
            verticalalignment='top',
            multialignment='center',
            bbox=props,
            label="Cuts")

        ax.legend(loc="upper right")
        applyFormatting(f,ax)
        #Residuals
        ax22.plot(x2,rise_fit.fun)
        ax22.set_xlim(ax.get_xlim())
        ax22.set_ylabel("Weight. Res")

    #saving
    if cr.save_fig:
        f.savefig(f'./figure/{cr.layer_name}_06_tw_fit.png')

    return (popt_fall,fall_err),(popt_rise,rise_err)

def chi2(fit):
    return np.sum(fit.fun**2)/(fit.fun.size -fit.x.size )

def get_error(fit_obj):
    J=fit_obj.jac
    pcov = np.linalg.inv(J.T.dot(J))
    par_err = np.sqrt(np.diag(pcov))
    return par_err
    
def runFor(dfs,debug=0): #need to set the Layer, u, v BE using setParameters()


    dfs = quickClean(dfs)
    if cr.parameters_set == 0:
        print("No parameters were set, using default values")


    if cr.save_fig:
        cr.layer_name = f"E{cr.BE}_L{cr.layer}_U{cr.u}_V{cr.v}"
        # print(cr.layer_name)   
    
    
    df = fixTOA(dfs,debug=debug)


    # Fits done with the initial cuts applied.
    
    X1 = (df.e) [df.tf_cut&(df.tf_f<fit_range[1])]
    X2 = (df.e) [df.tr_cut&(df.tr_f<fit_range[1])]
    Y1 = -((df.tf_f) - (df.tf_mcp_f))[df.tf_cut&(df.tf_f<fit_range[1])]
    Y2 = -((df.tr_f) - (df.tr_mcp_f))[df.tr_cut&(df.tr_f<fit_range[1])]
    bins=BINS_Fast
    
    a = fit(X1,Y1,X2,Y2,cr.layer,bins,debug=debug)


    X1 = (df.e) 
    X2 = (df.e) 
    Y1 = -((df.tf_f) - (df.tf_mcp_f))
    Y2 = -((df.tr_f) - (df.tr_mcp_f))


    cr.tw_fit_par_fall = a[0]
    cr.tw_fit_par_rise = a[1]

    p=a[0][0]
    TOF1 = Y1 - tw(X1,*p)
    cr.tw_fall_mean_200 = np.mean(TOF1[X1>200])
    TOF1=TOF1-cr.tw_fall_mean_200 #we need to keep track of this...
    
    
    p=a[1][0]
    TOF2 = Y2 - tw(X1,*p)
    cr.tw_rise_mean_200 = np.mean(TOF2[X2>200])
    TOF2=TOF2-cr.tw_rise_mean_200



    #saving the phase 1 cut so it can be applied in calculating the resolution
    df.tf_cut = (df.tf_f<fit_range[1])
    df.tr_cut = (df.tr_f<fit_range[1])




    df['tf_tof']    = TOF1
    df['tr_tof']    = TOF2




    # if cr.debug:
    #     plotTOF(df)
    #     timeRes_Edep(df)
    return df.dropna()
    

def timeRes_Edep(df): #Takes Calibrated TOF after runFor
    # The time resolution has to be derived after MCP amplitude cut.
    MCPAmplCut = (df.mcp_ampl > cr.set_mcp_ampl_cut)
    df = df[MCPAmplCut]
    def get_timeResBins(X,Y):
        # binx =np.concatenate((np.arange(0,400,80),np.arange(400,1600,220))) # This binning has to be exponential
        # base=6
        if type(cr.set_timeRes_Edep_binx) ==np.ndarray:
            cr.set_timeRes_Edep_binx=np.geomspace(20000, 170000, num=12)/100 -180
        binx = cr.set_timeRes_Edep_binx


        (_,tof     ),_,_ = binned_statistic(X, [X,Y], bins=binx, statistic=getGausStd)
        (_,tof_err ),_,_ = binned_statistic(X, [X,Y], bins=binx, statistic=getGausStdErr)
        (x,_       ),_,_ = binned_statistic(X, [X,Y], bins=binx, statistic="mean")
        (_,tof_count),_,_ = binned_statistic(X, [X,Y], bins=binx, statistic="count")
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(tof))&(tof != -999)&(np.invert(np.isnan(tof_err)))&(tof_err<5)&(np.invert(np.isnan(tof_count)))
        theCut&=(tof_count>30)
        x1 = x[theCut]
        y1 = tof[theCut]
        N = tof_count[theCut]
        y1_err = tof_err[theCut]+0.000001
        return (x1,y1,y1_err)


    f, axs = plt.subplots( figsize = (8,7))
    gs = GridSpec(2, 1, height_ratios=[6, 1])
    # defining the grid
    axs = plt.subplot(gs[0,0])
    ax1 = plt.subplot(gs[1,0])
    ax1.set_ylabel("Wei. Res")
    ax1.set_xlabel("Energy Deposited [MIPs]",fontsize=15)

    #FALL
    X = df.e[df.tf_cut] #Applying phase cuts...
    Y = df.tf_tof[df.tf_cut]
    values = get_timeResBins(X,Y)
    color,fit = cplts.plotTimeResvsEdep(*values,axs,label=f"FALL ",offset=0)

    cr.time_res_fit_fall = fit        
    ax1.scatter(values[0],fit.fun,color=color)

    #RISE
    X = df.e[df.tr_cut] #Applying phase cuts...
    Y = df.tr_tof[df.tr_cut]
    values = get_timeResBins(X,Y)
    color,fit = cplts.plotTimeResvsEdep(*values,axs,label="RISE",offset=0.4)
    cr.time_res_fit_rise = fit
    ax1.scatter(values[0],fit.fun,color=color)



    #Some labels
    axs.text(.7,1.1 ,f" MCP > {cr.set_mcp_ampl_cut}",
        transform=axs.transAxes,
        fontsize=16,
        verticalalignment='top',
        multialignment='center',
        # bbox=props,
        label="Cuts")

    applyFormatting(f,axs)
    if cr.save_fig:
        f.savefig(f'./figure/{cr.layer_name}_06_tof_res_edep.png')


    return None


def plotTOF(df): # Plotting the TOF after all corrections and MCP cut
    MCPAmplCut = (df.mcp_ampl > cr.set_mcp_ampl_cut)
    df = df[MCPAmplCut]

    X1 = df.e[df.tf_cut]
    Y1 = df.tf_tof[df.tf_cut]
    
    X2 = df.e[df.tr_cut]
    Y2 = df.tr_tof[df.tr_cut]


    f, ax = plt.subplots(1,2, figsize = (16,6))
    axs = ax[0]
    _=axs.hist2d(X1,Y1,
                bins=(np.arange(0,1600,10),np.arange(-5,5,0.05))
    #              bins=250
                
                ,norm=LogNorm())
    axs.set_xlabel("Edep [MIPS]",fontsize=15)
    axs.set_ylabel("Corrected TOF",fontsize=15)
    axs.set_title("Fall",fontsize=15)
    applyFormatting(f,axs)

    axs = ax[1]
    _=axs.hist2d(X2,Y2,
                bins=(np.arange(0,1600,10),np.arange(-5,5,0.05))
    #              bins=250
                
                ,norm=LogNorm())
    axs.set_xlabel("Edep [MIPS]",fontsize=15)
    axs.set_ylabel("Corrected TOF",fontsize=15)
    axs.set_title("RISE",fontsize=15)
    applyFormatting(f,axs)


    
    # axs.text(.6,1.07 ,f"{cr.BE}GeV e-; Layer {cr.layer}; @ ({cr.u},{cr.v})",
    # transform=axs.transAxes,
    # fontsize=16,
    # verticalalignment='top',
    # multialignment='center',
    # # bbox=props,
    # label="Cuts")
    return None




def get_labels(d): # Feed in list of numbers to get a string with labels
    st = ""
    lbl="abcdefghijklmnop"
    k=0
    for i in d:
        st+=f"{lbl[k]}:{round(i,3)}, "
        k=k+1
    return(st[:-2])

        

def pol(x,pars):# Function to calculate polynomial
    y=0
    j=0
    for i in pars:
        y+= x**(len(pars)-j-1)*i
        j+=1
    return y



def tw1(x,b,c,d):
    return  b + c / (x - d)


def cleanSigma(Y,sigma=2):
    cut = np.abs(Y-np.mean(Y))<sigma*np.std(Y)
    return cut


def gaus(x,a,x0,sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

def plotTW(eng,toa,mcp,title=""):
        # #Plotting Time Walk
        bins=BINS_Fast
        x,y = eng[toa>fit_range[1]],(toa-mcp)[toa>fit_range[1]]
        (x,y),_,_ = binned_statistic(x, [x,y], bins=bins, statistic="median")
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        x2 = x[theCut]
        y2 = y[theCut]
        x,y = eng[toa<fit_range[1]],(toa-mcp)[toa<fit_range[1]]
        (x,y),_,_ = binned_statistic(x, [x,y], bins=bins, statistic="median")
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        x1 = x[theCut]
        y1 = y[theCut]
        f, ax = plt.subplots(1,1, figsize = (8,6))
        # ax.set_title(title)
        _,binx,biny,quad_mesh=ax.hist2d(eng,(toa-mcp),bins=bins,norm=LogNorm())
        _=f.colorbar(quad_mesh, ax=ax)
        ax.scatter(x1,y1,label=f"Linear Region [X-Profile]")
        ax.scatter(x2,y2,label=f"Non-Linear Region [X-Profile]")

        ax.set_xlabel("Energy [MIP]")
        ax.set_ylabel("TOA - MCP [ns]")
        ax.legend(loc='upper right')
        applyFormatting(f,ax)

        if cr.save_fig:
            f.savefig(f'./figure/{cr.layer_name}_{title}.png')

        

def getGausStd(TOF1,debug=0):
    try:
        #bins=np.arange(-2,2,0.05)
        if type(cr.set_getGausStd_binx) is np.ndarray:

            cr.set_getGausStd_binx = np.arange(-2,2,0.05)

        bins= cr.set_getGausStd_binx

        y,bins = np.histogram(TOF1,bins=bins)
        x = bins[:-1]+(bins[1]-bins[0])/2
        mean = sum(x * y) / sum(y)
        sigma = np.sqrt(sum(y * (x - mean)**2) / sum(y))

        # _ = axs.scatter(x,y,color="orange")
        popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
        fall_par = popt
        fall_err = np.sqrt(np.diag(pcov))
        if debug==1:
            f,ax = plt.subplots(figsize=(8,6))
            _ = ax.hist(TOF1,bins=bins)
            _=ax.plot(x,gaus(x,*popt),'r--',label='fit')
            #axs.set_xlim(8,14)
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)

            ax.set_xlabel("TOF(ns)",fontsize=15)
            text = f"sig = {np.abs(round(fall_par[2],3))} +/- {np.abs(round(fall_err[2],3))}\n"
            text +=f"mu = {(round(fall_par[1],3))} +/- {np.abs(round(fall_err[1],3))}"
            ax.text(0.6, 0.8,text,
             transform=ax.transAxes,
             fontsize=12,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")
        return np.abs(fall_par[2])
    except:
        return -999

def getGausStdErr(TOF1,debug=0):
    try:
        if type(cr.set_getGausStd_binx) is np.ndarray:
            cr.set_getGausStd_binx = np.arange(-2,2,0.05)
        bins= cr.set_getGausStd_binx
        y,bins = np.histogram(TOF1,bins=bins)
        x = bins[:-1]+(bins[1]-bins[0])/2
        mean = sum(x * y) / sum(y)
        sigma = np.sqrt(sum(y * (x - mean)**2) / sum(y))

        # _ = axs.scatter(x,y,color="orange")
        popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
        fall_par = popt
        fall_err = np.sqrt(np.diag(pcov))
        if debug==1:
            f,ax = plt.subplots(figsize=(8,6))
            _ = ax.hist(TOF1,bins=bins)
            _=ax.plot(x,gaus(x,*popt),'r--',label='fit')
            #axs.set_xlim(8,14)
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)

            ax.set_xlabel("TOF(ns)",fontsize=15)
            text = f"sig = {np.abs(round(fall_par[2],3))} +/- {np.abs(round(fall_err[2],3))}\n"
            text +=f"mu = {(round(fall_par[1],3))} +/- {np.abs(round(fall_err[1],3))}"
            ax.text(0.6, 0.8,text,
             transform=ax.transAxes,
             fontsize=12,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")
        return np.abs(fall_err[2])
    except:
        return -999

def getGausMean(TOF1,debug=0):
    try:
        #bins=np.arange(-2,2,0.05)
        bins= cr.set_getGausStd_binx
        y,bins = np.histogram(TOF1,bins=bins)
        x = bins[:-1]+(bins[1]-bins[0])/2
        mean = sum(x * y) / sum(y)
        sigma = np.sqrt(sum(y * (x - mean)**2) / sum(y))

        # _ = axs.scatter(x,y,color="orange")
        popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
        fall_par = popt
        fall_err = np.sqrt(np.diag(pcov))
        if debug==1:
            f,ax = plt.subplots(figsize=(8,6))
            _ = ax.hist(TOF1,bins=bins)
            _=ax.plot(x,gaus(x,*popt),'r--',label='fit')
            #axs.set_xlim(8,14)
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)

            ax.set_xlabel("TOF(ns)",fontsize=15)
            text = f"sig = {np.abs(round(fall_par[2],3))} +/- {np.abs(round(fall_err[2],3))}\n"
            text +=f"mu = {(round(fall_par[1],3))} +/- {np.abs(round(fall_err[1],3))}"
            ax.text(0.6, 0.8,text,
             transform=ax.transAxes,
             fontsize=12,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")
        return np.abs(fall_par[1])
    except:
        return -999

def getGausMeanErr(TOF1,debug=0):
    try:
        #bins=np.arange(-2,2,0.05)
        bins= cr.set_getGausStd_binx

        y,bins = np.histogram(TOF1,bins=bins)
        x = bins[:-1]+(bins[1]-bins[0])/2
        mean = sum(x * y) / sum(y)
        sigma = np.sqrt(sum(y * (x - mean)**2) / sum(y))

        # _ = axs.scatter(x,y,color="orange")
        popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
        fall_par = popt
        fall_err = np.sqrt(np.diag(pcov))
        if debug==1:
            f,ax = plt.subplots(figsize=(8,6))
            _ = ax.hist(TOF1,bins=bins)
            _=ax.plot(x,gaus(x,*popt),'r--',label='fit')
            #axs.set_xlim(8,14)
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)

            ax.set_xlabel("TOF(ns)",fontsize=15)
            text = f"sig = {np.abs(round(fall_par[2],3))} +/- {np.abs(round(fall_err[2],3))}\n"
            text +=f"mu = {(round(fall_par[1],3))} +/- {np.abs(round(fall_err[1],3))}"
            ax.text(0.6, 0.8,text,
             transform=ax.transAxes,
             fontsize=12,
             verticalalignment='top',
             multialignment='center',
             bbox=props,
             label="Cuts")
        return np.abs(fall_err[1])
    except:
        return -999


def get68_Std(data):
    if data.size < 10:
        return -999
    dt = data - np.mean(data)
    limit = 0.683
    min_increase = 0.002 
    window = np.std(dt)/4
    total_size = dt.size
    frac_pop = 0
    while (frac_pop < limit ):
        theCut = (np.abs(dt)< window)
        frac_pop = (dt[theCut].size) / total_size
        window += min_increase + .1*((limit-0.001)-frac_pop )
    #print(frac_pop)        
    sigma = (np.max(data[theCut]) - np.min(data[theCut]))/2
    
    return sigma
    
def setMCP(data,mcp_no):
    if mcp_no not in [1,2]:
        print("MCP Number outside bounds; MCP not selected")
        return data
    else:
        data['mcp'] = data[f'mcp_{mcp_no}']
        data['mcp_valid'] = data[f'mcp_valid_{mcp_no}']
        data['mcp_ampl'] = data[f'mcp_ampl_{mcp_no}']
        
        return data

def applyFormatting(fig,ax):
    # pass
    #Formatting
    ax.text(.1, 0.95, r"$\bf{CMS}$ Preliminary",
         transform=fig.transFigure,
         fontsize=18,
         verticalalignment='top',
         multialignment='center',
         # bbox=props,
         # zorder=15,
         label="cms")

    ax.grid()
    ax.minorticks_on()
    # ax.tick_params(axis='both', left='True', top=False, right='on',
    #                 bottom='True', labelleft='True', labeltop='False',
    #                 labelright='False', labelbottom='True')
    # textDisp = str(eng) + "GeV Electron"
    # _ = ax.text(0.73, .93,textDisp,
    #   transform=fig.transFigure,
    #   fontsize=14,
    #   verticalalignment='top',
    #   multialignment='center',
    #   # bbox=props,
    #   # zorder=15,
    #   label="lumi")