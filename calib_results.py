
#set of globals that can be used to retrieve calibration information


#append to this list
vars = {
    #### Internal Pars
    "parameters_set":0, # Dont Touch
    #### End of Internal Pars


    #### Variables to set
    'layer':9,#Need
    'u':-2,#Need
    'v':1,#Need
    'BE':300,#Need
    'set_lin_pars':'111',#Need
    'debug':0,#Need
    'save_fig':0,
    'layer_name':0,
    'set_mcp_ampl_cut':500,#Need

    #This is the bins in the Gassian 1D histograms
    'set_getGausStd_binx':999,#Need

    #This is the x-axis binning in the Time Res vs Edep  2D histograms
    'set_timeRes_Edep_binx':999, #Need


    #### END OF Variables to set

    

    #### Calibration Variables

    #MergePop Vars
    "mergePop_offset":999,

    #Linearizer Vars

    #Polynomial Correction Variables
    "lin_linear_par_1":999,
    "lin_poly_par_1":999,
    #Slope Correction Variables
    #This is just after the polynomial correction...
    #Includes error...
    # This is is the variable for slope correction
    "lin_linear_par_2":999, #redundant with par_1 above
    "lin_poly_par_2":999, 
    #after slope correction, this intercept is used to correct for the slope
    #fit over full range, not fit range...
    "lin_linear_par_3":999, 
    "lin_poly_par_3":999, 

    #TW_Correction

    #Fit Parameters with error
    'tw_fit_par_fall':999,
    'tw_fit_par_rise':999,
    #We subtract the mean of the TW curve Edep > 200
    "tw_fall_mean_200":999,
    "tw_rise_mean_200":999,
    "tw_fit_fall":999,
    "tw_fit_rise":999,
    #Derivation of Time_Res Edep Fit
    "time_res_fit_fall":999,
    "time_res_fit_rise":999,
    
    #### END of Calibration Variables

    }


#Setting the var values
for key in vars.keys():
    exec(f'{key} = {vars[key]}')



def rst():
    global vars
    for key in vars.keys():
        globals()[key]=vars[key]
    # print("all global parameters are reset")
    # print_vars()
     

def print_vars(limit = None):
    ind=1
    for i in globals():
        if  not any(value in i for value in ['rst',"__",'vars']) :
            print("%20s \t %10s"%(i,str(globals()[i])))
            if(limit != None):
                ind=ind+1
                if(ind >limit):
                    break





def get_vars():
    values = {}
    global vars
    for key in vars.keys():
        values[key] = globals()[key]
    return values