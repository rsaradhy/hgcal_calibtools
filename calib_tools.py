import uproot as up
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats as st
import pandas as pd
from matplotlib.colors import LogNorm
import matplotlib as mpl
import pylandau as landau
mpl.rcParams['image.cmap'] = 'inferno'
from scipy.optimize import curve_fit
from scipy.stats import iqr, norm, binned_statistic, binned_statistic_2d
from scipy.special import erf



def test():
    version = 1.2
    print(f"Calib Toolkit Version {version}")

# function to merge
#This function only work is the populations are not close to 45 degree line
def merge2pop(toa,mcp,energy,debug=0,label="",polFix=0):
    bins=500
    #General Cuts :: Looking at energy > 200
    engCut =100
    cuts2 = (energy>=engCut)&(toa>0)&(toa<25)&(mcp>0)&(mcp<25)
    #clean up nans
    cleanNAN = np.invert(np.isnan(toa))&np.invert(np.isnan(mcp))&np.invert(np.isnan(energy))
    #applying cuts and creating working vars
    w_toa = toa[cuts2&cleanNAN]
    w_mcp = mcp[cuts2&cleanNAN]
    w_energy = energy[cuts2&cleanNAN]
    
    if debug:
        f, ax = plt.subplots( figsize = (8,6))
        ax.hist2d(mcp,toa,bins=100,norm=LogNorm())
        ax.set_title(f"{label}: All Energy")
        ax.set_xlabel("MCP [ns]")
        ax.set_ylabel("TOA [ns]")
        f, ax = plt.subplots( figsize = (8,6))
        ax.hist2d(w_mcp,w_toa,bins=100,norm=LogNorm())
        ax.set_title(f"{label}: Energy>{engCut} MIPs")
        ax.set_xlabel("MCP [ns]")
        ax.set_ylabel("TOA [ns]")

    
    
    
    #Separating the populations
    left_X  = w_mcp[w_toa>w_mcp]
    left_Y  = w_toa[w_toa>w_mcp]
    right_X  = w_mcp[w_toa<w_mcp]
    right_Y  = w_toa[w_toa<w_mcp]


    #Clean up within 3 sigma
    left_tof = left_Y-left_X
    left_std = np.std(left_tof)
    left_mean = np.mean(left_tof)
    left_X = left_X[np.abs(left_tof-left_mean) < 3*left_std ] 
    left_Y = left_Y[np.abs(left_tof-left_mean) < 3*left_std ] 
    right_tof = right_Y-right_X
    right_std = np.std(right_tof)
    right_mean = np.mean(right_tof)
    right_X = right_X[np.abs(right_tof-right_mean) < 3*right_std ] 
    right_Y = right_Y[np.abs(right_tof-right_mean) < 3*right_std ] 







    (x1,y1),_,_ = binned_statistic(left_X, [left_X,left_Y], bins=bins, statistic="mean")
    (x2,y2),_,_ = binned_statistic(right_X, [right_X,right_Y], bins=bins, statistic="mean")

    theCut1 = (np.invert(np.isnan(x1)))&(np.invert(np.isnan(y1)))
    theCut2 = (np.invert(np.isnan(x2)))&(np.invert(np.isnan(y2)))

    x1 = x1[theCut1]
    x2 = x2[theCut2]
   
    y1 = y1[theCut1]
    y2 = y2[theCut2]
    
    #Interval to ignore
    x1_max = np.max(x1)
    x2_min = np.min(x2)
    
    
    
    if debug:

        #plotting 
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}: X-Profile Energy>{engCut} MIPs")


        #Data to fit
        ax.scatter(x1[x1<x2_min],y1[x1<x2_min])
        ax.scatter(x2[x2>x1_max],y2[x2>x1_max])
    
    #pol1
    par1 = np.polyfit(x1[x1<x2_min],y1[x1<x2_min],1)
    par2 = np.polyfit(x2[x2>x1_max],y2[x2>x1_max],1)
    shift_up = pol(x1_max,par1) - pol(x1_max,par2)
    
    
    


    #pol2
    par11 = np.polyfit(x1[x1<x2_min],y1[x1<x2_min],2)
    par22 = np.polyfit(x2[x2>x1_max],y2[x2>x1_max],2)

    #     shift_up = pol(x1_max,par11) - pol(x1_max,par22)

    #pol3
    # par111 = np.polyfit(x1[x1<x2_min],y1[x1<x2_min],3)
    # par222 = np.polyfit(x2[x2>x1_max],y2[x2>x1_max],3)
    
    
    if debug:
        print(par11)
        print(par22)
        print(shift_up)

        #Data to consider separately
        ax.scatter(x2[x2<x1_max],y2[x2<x1_max],color='green')    
        ax.scatter(x1[x1>x2_min],y1[x1>x2_min],color='green')


        _=ax.axvline(x1_max,color='orange',linestyle='--')
        _=ax.axvline(x2_min,color='orange',linestyle='--')
    
    
    
    
    
        #Plotting the  pol1
        ax.plot(x1,pol(x1,par1),color='red',linestyle="--",label=f"pol-1 left: a={round(par1[0],3)}, b={round(par1[1],3)}",linewidth=2)
        ax.plot(x2,pol(x2,par2),color='red',linestyle="--",label=f"pol-1 right: a={round(par2[0],3)}, b={round(par2[1],3)}",linewidth=2)


        ax.plot(x1,pol(x1,par11),color='black',linestyle="--",label=f"pol-2 left: a={round(par11[0],3)}, b={round(par11[1],3)}, c={round(par11[2],3)} ",linewidth=2)
        ax.plot(x2,pol(x2,par22),color='black',linestyle="--",label=f"pol-2 right: a={round(par22[0],3)}, b={round(par22[1],3)}, c={round(par22[2],3)}",linewidth=2)


        ax.plot(w_mcp,par2[0]*w_mcp+(par2[1]+par1[1])/2,linestyle=':',label="population seperator")
        ax.set_xlabel("MCP [ns]")
        ax.set_ylabel("TOA [ns]")
    
    
    slope=par2[0]
    intercept= (par2[1]+par1[1])/2
    
    
    #     ax.plot(x1,x1**3*par111[0]+ x1**2*par111[1]+ x1*par111[2]+par111[3],color='white',linestyle="--",label="pol-3 left ",linewidth=2)
    #     ax.plot(x2,x2**3*par222[0]+ x2**2*par222[1]+ x2*par222[2]+par222[3],color='white',linestyle="--",label="pol-3 right",linewidth=2)
    
    
    
    toa_fixed = w_toa + (w_toa < w_mcp*slope +intercept)*(shift_up/2) # increase right side
    toa_fixed -= (w_toa > w_mcp*slope +intercept)*(shift_up/2) # lower left side
    
    # pol fix
    if polFix:
        toa_fixed -= (w_toa > w_mcp*slope +intercept)*(pol(w_mcp,par11)-pol(w_mcp,par1)) # left side pol fix
        toa_fixed -= (w_toa < w_mcp*slope +intercept)*(pol(w_mcp,par22)-pol(w_mcp,par2)) # right side pol fix

    
    if debug:
        ax.legend(loc='lower left')
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}: Energy>{engCut} MIPs ,polfix = {polFix}")
        ax.hist2d(w_mcp,toa_fixed,bins=100,norm=LogNorm())
        ax.set_xlabel("MCP [ns]")
        ax.set_ylabel("TOA [ns]")

    
    toa_fixed = toa + ((toa < mcp*slope +intercept))*(shift_up/2) # increase right side
    toa_fixed -= (toa > mcp*slope +intercept)*(shift_up/2) # lower left side
    
    if debug:
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}:All energy TIME WALK without pol correction")
        ax.hist2d(energy,toa_fixed-mcp,bins=100,norm=LogNorm())
        ax.set_xlabel("Energy [MIP]")
        ax.set_ylabel("TOA - MCP [ns]")
    
    # pol fix
    if polFix:
        toa_fixed -= (toa > mcp*slope +intercept)*(pol(mcp,par11)-pol(mcp,par1)) # left side pol fix
        toa_fixed -= (toa < mcp*slope +intercept)*(pol(mcp,par22)-pol(mcp,par2)) # right side pol fix
    
    
    if (debug&polFix):
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}: All energy TIME WALK with pol correction")
        ax.hist2d(energy,toa_fixed-mcp,bins=100,norm=LogNorm())
        ax.set_xlabel("Energy [MIP]")
        ax.set_ylabel("TOA - MCP [ns]")
        
        
    
    if debug:    
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}: all energy,polfix = {polFix}")
        ax.hist2d(mcp,toa_fixed,bins=100,norm=LogNorm())
        ax.set_xlabel("Energy [MIP]")
        ax.set_ylabel("TOA - MCP [ns]")
    
    return toa_fixed

def clean(toa,mcp,energy,sig=2,debug=0,label=""): # removes values which are 2 sigma away and removes nans
    # bins=500
    #     debug=1
    #     print(sig)
    #clean up nans
    cleanNAN = np.invert(np.isnan(toa))&np.invert(np.isnan(mcp))&np.invert(np.isnan(energy))&(mcp>0)&(mcp<25)
    #applying cuts and creating working vars
    w_toa = toa[cleanNAN]
    w_mcp = mcp[cleanNAN]
    w_energy = energy[cleanNAN]
    
    #     f, ax = plt.subplots( figsize = (8,6))
    #     ax.hist2d(w_mcp,w_toa,bins=100,norm=LogNorm())
    
    
    #Clean out anything lying 2 Sigma  away
    numstd = sig
    Y = w_toa-w_mcp
    stdY = np.std(Y)
    low,high = np.mean(Y)- numstd*stdY,np.mean(Y)+ numstd*stdY
    Y_cut = (Y>low)&(Y<high)
    w_mcp = w_mcp[Y_cut]
    w_toa = w_toa[Y_cut]
    w_energy = w_energy[Y_cut]
    if debug:
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}: Before Clean up Sig={sig}")

        ax.hist2d(mcp[energy>20],toa[energy>20],bins=100,norm=LogNorm())
        
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}:Before TIME WALK")
        ax.hist2d(energy,toa-mcp,bins=100,norm=LogNorm())
        ax.set_xlabel("Energy [MIP]")
        ax.set_ylabel("TOA - MCP [ns]")


        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}: After CleanUp Sig={sig}")

        ax.hist2d(w_mcp[w_energy>20],w_toa[w_energy>20],bins=100,norm=LogNorm())
        
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}:TIME WALK")
        ax.hist2d(w_energy,w_toa-w_mcp,bins=100,norm=LogNorm())
        ax.set_xlabel("Energy [MIP]")
        ax.set_ylabel("TOA - MCP [ns]")
        
        
    data = {
                'mcp':w_mcp,
                'toa':w_toa,
                'eng':w_energy
           }


        
    
    
    return data

    
def merger(toa,mcp,energy,sig=2,debug=0,label="",polFix=0): #this is the function used to seperate the two algorithms...
    if (np.std((toa-mcp)[energy>200]) > 8 ):
        toa_fixed = merge2pop(toa,mcp,energy,debug,label,polFix)
        sig =6
        return clean(toa_fixed,mcp,energy,sig,debug,label)
    else:
        return clean(toa,mcp,energy,sig,debug,label)


def lineariser(data,doIt=False,debug=0,correctSlope=1,correctIntercept=1,label=""):
    
    toa = data['toa']
    mcp = data['mcp']    
    eng = data['eng']
    

    energy_cut = 200
    # w_eng = eng[eng>energy_cut]
    w_toa = toa[eng>energy_cut]
    w_mcp = mcp[eng>energy_cut]
    
    
    
    bins=100
    if debug:
        pass
    # f, ax = plt.subplots( figsize = (8,6))
    # ax.hist2d(mcp,toa,bins=100,norm=LogNorm())
    # ax.set_xlabel("MCP [ns]")
    # ax.set_ylabel("TOA [ns]")
    
    (x,y),_,_ = binned_statistic(w_mcp, [w_mcp,w_toa], bins=bins, statistic="mean")
    theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
    x = x[theCut]
    y = y[theCut]
    par1 = np.polyfit(x,y,1)
    par2 = np.polyfit(x,y,2)
    par3 = np.polyfit(x,y,3)
    
    if debug:
        f, ax = plt.subplots( figsize = (8,6))
        ax.hist2d(mcp,toa,bins=100,norm=LogNorm())
        ax.set_xlabel("MCP [ns]")
        ax.set_ylabel("TOA [ns]")
        ax.scatter(x,y,color="yellow")
        ax.plot(x,pol(x,par1),color="red",linestyle="--",  label=f'pol1 a={round(par1[0],3)}, b={round(par1[1],3)}')
        ax.plot(x,pol(x,par2),color="green",linestyle="--",label=f'pol2 a={round(par2[0],3)}, b={round(par2[1],3)}, c={round(par2[2],3)}')
        # ax.plot(x,pol(x,par3),color="black",linestyle="--",label=f'pol3 a={round(par3[0],3)}, b={round(par3[1],3)}, c={round(par3[2],3)}, d={round(par3[3],3)}')
        ax.legend(loc="upper left")
        ax.set_title(f"{label}:Before Correction")
        f, ax = plt.subplots( figsize = (8,6))
        ax.set_title(f"{label}:TIME WALK before correction")
        ax.hist2d(eng,toa-mcp,bins=100,norm=LogNorm())
        ax.set_xlabel("Energy [MIP]")
        ax.set_ylabel("TOA - MCP [ns]")

        

        
        
    if doIt:
        toa -= (pol(mcp,par2)-pol(mcp,par1))
        w_toa = toa[eng>energy_cut]
        w_mcp = mcp[eng>energy_cut]
        (x,y),_,_ = binned_statistic(w_mcp, [w_mcp,w_toa], bins=bins, statistic="mean")
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        x = x[theCut]
        y = y[theCut]
        par1,par1_cov = np.polyfit(x,y,1,cov=True)
        data['par1'] = (par1,np.sqrt(np.diag(par1_cov)))

        # print (np.sqrt(np.diag(par1_cov)))
        
        par2 = np.polyfit(x,y,2)
        
        if debug:
            f, ax = plt.subplots( figsize = (8,6))
            ax.hist2d(mcp,toa,bins=100,norm=LogNorm())
            ax.set_xlabel("MCP [ns]")
            ax.set_ylabel("TOA [ns]")
            ax.scatter(x,y,color="yellow")
            ax.plot(x,pol(x,par1),color="red",linestyle="--",  label=f'pol1 a={round(par1[0],3)}, b={round(par1[1],3)}')
            ax.plot(x,pol(x,par2),color="green",linestyle="--",label=f'pol2 a={round(par2[0],3)}, b={round(par2[1],3)}, c={round(par2[2],3)}')
            #ax.plot(x,pol(x,par3),color="black",linestyle="--",label=f'pol3 a={round(par3[0],3)}, b={round(par3[1],3)}, c={round(par3[2],3)}, d={round(par3[3],3)}')
            ax.legend(loc="upper left")
            ax.set_title(f"{label}:After Correction to pol1")
            #Plotting Time Walk
            f, ax = plt.subplots( figsize = (8,6))
            ax.set_title(f"{label}:TIME WALK after pol1 correction")
            ax.hist2d(eng,toa-mcp,bins=100,norm=LogNorm())
            ax.set_xlabel("Energy [MIP]")
            ax.set_ylabel("TOA - MCP [ns]")
        
    
            
        
    if correctSlope:
        avg_mcp = np.mean(mcp)
        # print(avg_mcp)
        toa -= (pol(mcp-avg_mcp,par1) - pol(mcp-avg_mcp,(1,par1[1])))
        w_toa = toa[eng>energy_cut]
        w_mcp = mcp[eng>energy_cut]
        (x,y),_,_ = binned_statistic(w_mcp, [w_mcp,w_toa], bins=bins, statistic="mean")
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        x = x[theCut]
        y = y[theCut]
        par1,par1_cov = np.polyfit(x,y,1,cov=True)
        data['par1_after_slope'] = (par1,np.sqrt(np.diag(par1_cov)))





        par2 = np.polyfit(x,y,2)
        # par3 = np.polyfit(x,y,3)
        
        if debug:        
            f, ax = plt.subplots( figsize = (8,6))
            ax.hist2d(mcp,toa,bins=100,norm=LogNorm())
            ax.set_xlabel("MCP [ns]")
            ax.set_ylabel("TOA [ns]")
            ax.scatter(x,y,color="yellow")
            ax.plot(x,pol(x,par1),color="red",linewidth=2,linestyle="--",  label=f'pol1 a={round(par1[0],3)}, b={round(par1[1],3)}')
            ax.plot(x,pol(x,par2),color="green",linestyle="--",label=f'pol2 a={round(par2[0],3)}, b={round(par2[1],3)}, c={round(par2[2],3)}')
            ax.plot(x,pol(x,par3),color="black",linestyle="--",label=f'pol3 a={round(par3[0],3)}, b={round(par3[1],3)}, c={round(par3[2],3)}, d={round(par3[3],3)}')
            ax.legend(loc="upper left")
            ax.set_title(f"{label}:After Correction of Slope")
            
            #Plotting Time Walk
            f, ax = plt.subplots( figsize = (8,6))
            ax.set_title(f"{label}:TIME WALK after slope correction")
            ax.hist2d(eng,toa-mcp,bins=200,norm=LogNorm())
            ax.set_xlabel("Energy [MIP]")
            ax.set_ylabel("TOA - MCP [ns]")



    if correctIntercept:
        #Correcting Intercept
        toa -= par1[1]
        w_toa = toa[eng>energy_cut]
        w_mcp = mcp[eng>energy_cut]
        (x,y),_,_ = binned_statistic(w_mcp, [w_mcp,w_toa], bins=bins, statistic="mean")
        theCut = np.invert(np.isnan(x))&np.invert(np.isnan(y))
        x = x[theCut]
        y = y[theCut]
        par1 = np.polyfit(x,y,1)
        par2 = np.polyfit(x,y,2)
        par3 = np.polyfit(x,y,3)
        
        if debug:        
            f, ax = plt.subplots( figsize = (8,6))
            ax.hist2d(mcp,toa,bins=100,norm=LogNorm())
            ax.set_xlabel("MCP [ns]")
            ax.set_ylabel("TOA [ns]")
            ax.scatter(x,y,color="yellow")
            ax.plot(x,pol(x,par1),color="red",linewidth=2,linestyle="--",  label=f'pol1 a={round(par1[0],3)}, b={round(par1[1],3)}')
            ax.plot(x,pol(x,par2),color="green",linestyle="--",label=f'pol2 a={round(par2[0],3)}, b={round(par2[1],3)}, c={round(par2[2],3)}')
            # ax.plot(x,pol(x,par3),color="black",linestyle="--",label=f'pol3 a={round(par3[0],3)}, b={round(par3[1],3)}, c={round(par3[2],3)}, d={round(par3[3],3)}')
            ax.legend(loc="upper left")
            ax.set_title(f"{label}:After Correction of Intercept Correction")
            
            #Plotting Time Walk
            f, ax = plt.subplots( figsize = (8,6))
            ax.set_title(f"{label}:TIME WALK after Intercept Correction")
            ax.hist2d(eng,toa-mcp,bins=200,norm=LogNorm())
            ax.set_xlabel("Energy [MIP]")
            ax.set_ylabel("TOA - MCP [ns]")
        pass

        
        
        
        
    data['toa'] = toa
    data['mcp'] = mcp 
    data['eng'] = eng
    
    

    
    return data
    

def pol(x,pars):# Function to calculate polynomial
    y=0
    j=0
    for i in pars:
        y+= x**(len(pars)-j-1)*i
        j+=1
    return y


def collectData(Layer,data_rec,data_MCP,XY=(None, None),debug=0):
    layer = data_rec.array('rechit_layer')
    energy = data_rec.array('rechit_energy')[layer == Layer]
    x = data_rec.array('rechit_x')[layer == Layer]
    y = data_rec.array('rechit_y')[layer == Layer]
    #Cell in the first event with max energy; this needs to be fixed!
    if XY==(None,None):
        target_cellx = []
        target_celly = []
        for event in range(0,1000): #look in 1000 events
            target_cellx.append(x[event][((energy[event]==np.max(energy[event])))][0])
            target_celly.append(y[event][((energy[event]==np.max(energy[event])))][0])
        target_cellx = st.mode(target_cellx)[0][0]
        target_celly = st.mode(target_celly)[0][0]
    else:
        target_cellx = XY[0]
        target_celly = XY[1]
    
    if debug:
        print(target_cellx,target_celly)



    #time 
    time_fall = data_rec.array('rechit_toaFall_time')[layer == Layer]
    time_rise = data_rec.array('rechit_toaRise_time')[layer == Layer]

    #mcp_time 
    mcp_time = data_MCP.array(b'TS_toClock_FE_MCP1')
    mcp_valid = data_MCP.array(b'valid_TS_MCP1')

    #cuts
    mcp_cut = ((np.invert(np.isnan(mcp_time)))&(mcp_valid ==1)&(mcp_time < 40)&(mcp_time>0))
    cuts = (x==target_cellx)&(y==target_celly)&mcp_cut&(energy>30)



    #creating a mask for events with no hits in this cell
    event_selector = []
    for i in time_fall[cuts]:
        event_selector.append(bool(i.size))
        
        

    energy = np.concatenate(energy[cuts])
    toa_fall = np.concatenate(time_fall[cuts]) 
    toa_rise = np.concatenate(time_rise[cuts]) 
    mcp= mcp_time[event_selector&mcp_cut] 

    data= {
        'eng':energy,
        'fall':toa_fall,
        'rise':toa_rise,
        'mcp':mcp,
        'layer':Layer
    }
    return data

def tw(x,a,b,c,d):
    return a*x + b + c / (x - d)

def fit(X1,Y1,X2,Y2,Layer,bins):
    f, axs = plt.subplots(1,2, figsize = (16,6))
    axs[0].hist2d(X1,Y1,bins,norm=LogNorm())
    axs[1].hist2d(X2,Y2,bins,norm=LogNorm())
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)


    (x11,toa_fall),_,_ = binned_statistic(X1, [X1,Y1], bins=bins, statistic="mean")
    x1 = x11[np.invert(np.isnan(x11))&np.invert(np.isnan(toa_fall))]
    toa_fall = toa_fall[np.invert(np.isnan(x11))&np.invert(np.isnan(toa_fall))]


    (x22,toa_rise),_,_ = binned_statistic(X2, [X2,Y2], bins=bins, statistic='mean')
    x2 = x22[np.invert(np.isnan(x22))&np.invert(np.isnan(toa_rise))]
    toa_rise = toa_rise[np.invert(np.isnan(x22))&np.invert(np.isnan(toa_rise))]


    popt_fall, pcov_fall = curve_fit(tw, x1, toa_fall,p0=(0.001,3,320,0))
    popt_rise, pcov_rise = curve_fit(tw, x2, toa_rise,p0=(0.001,3,320,0))
    parameter_name = ['a','b','c','d']
    textstr = r"$f(x)=ax +b +\frac{c}{x-d}$"+"\n"

    #popt_fall, pcov_fall = curve_fit(tw, x1, toa_fall,p0=(2,300,2))
    #popt_rise, pcov_rise = curve_fit(tw, x2, toa_rise,p0=(2,300,2))
    #parameter_name = ['b','c','d']
    #textstr = r"$f(x)=b +\frac{c}{x-d}$"+"\n"
    
    fall_err = np.sqrt(np.diag(pcov_fall))
    rise_err = np.sqrt(np.diag(pcov_rise))
    


    r0 = axs[0].scatter(x1,toa_fall,marker="*")
    axs[0].set_ylabel("TOA Fall-MCP (ns)")
    axs[0].set_xlabel("Energy (MIPs)")
    axs[0].plot(
                x1, tw(x1,*popt_fall)
                ,color='green'
                ,linewidth='3'
                ,linestyle='--'
    )
    
    axs[1].set_title(f"Layer = {Layer}")
    axs[0].set_title(f"Layer = {Layer}")
    
    
    
    
    for i,j,k in zip(popt_fall,parameter_name,fall_err):
        textstr+=f" {j} = {round(i,3)}"+r"$\pm$"+f"{round(k,3)}\n"
        
    
            
    axs[0].text(0.5, 0.95,textstr,
         transform=axs[0].transAxes,
         fontsize=10,
         verticalalignment='top',
         multialignment='center',
         bbox=props,
         label="Cuts")
    



    r1 = axs[1].scatter(x2,toa_rise,marker="*")
    axs[1].set_ylabel("TOA Rise-MCP (ns)")
    axs[1].set_xlabel("Energy (MIPs)")
    axs[1].plot(
                x2, tw(x2,*popt_rise)
                ,color='green'
                ,linewidth='3'
                ,linestyle='--'
    )
    
    
    textstr = r"$f(x)=ax +b +\frac{c}{x-d}$"+"\n"
    
    for i,j,k in zip(popt_rise,parameter_name,rise_err):
        textstr+=f" {j} = {round(i,3)}"+r"$\pm$"+f"{round(k,3)}\n"
            
    axs[1].text(0.5, 0.95,textstr,
         transform=axs[1].transAxes,
         fontsize=10,
         verticalalignment='top',
         multialignment='center',
         bbox=props,
         label="Cuts")
    return (popt_rise,rise_err),(popt_fall,fall_err)

def gaus(x,a,x0,sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))